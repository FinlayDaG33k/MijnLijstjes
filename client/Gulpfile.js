var gulp = require('gulp');
var sass = require('gulp-sass');

// Create a task to compile our SASS-styles
gulp.task('styles', function() {
  // Create a watch for sass/**/*.scss (any scss file in sass/*)
  gulp.src('www/sass/**/*.scss')
    // Log errors
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    // The output destination for our compiled *.scss file (will put it in css/*.css)
    .pipe(gulp.dest('www/css/'));
});

// Create a task to watch
gulp.task('default',function() {
  // Compile css on start
  gulp.start('styles');
  // Watch the sass/ folder for changes
  gulp.watch('www/sass/**/*.scss',['styles']);
});
