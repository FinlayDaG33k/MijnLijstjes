// Creating Localforage Instance
var appSettings = localforage.createInstance({
  name: "appSettings"
});

// Creating Localforage Instance
var userData = localforage.createInstance({
  name: "userData"
});

// Function to get the UID out of the localstorage
async function getUID(){
  return new Promise(function(resolve,reject){
    userData.getItem('UID').then(function(value){
      resolve(value);
    })
    .catch(function(err) {
      console.log(err);
      reject();
    });
  });
}

// Function to get the Authtoken out of the localstorage
async function getAuthtoken(){
  return new Promise(function(resolve,reject){
    userData.getItem('Authtoken').then(function(value){
      resolve(value);
    })
    .catch(function(err) {
      console.log(err);
      reject();
    });
  });
}
