$(function(){
  $(".button-collapse").sideNav({
    menuWidth: 300,
    closeOnClick: true,
    edge: 'right', // <--- CHECK THIS OUT
  });
});


/* Function to log to the console (a bit overkill, but makes it easier to figure out what is from MijnLijstjes and what isn't) */
$.log = function(message){
  if(appDebug){
    console.log("[MijnLijstjes] " + message);
  }
}

$.showLoader = function(loaderID,contentID){
  $("#"+loaderID).show();
  $("#"+contentID).hide();
}

$.hideLoader = function(loaderID,contentID){
  $("#"+contentID).show();
  $("#"+loaderID).hide();
}

// Function to check validity of authtoken
async function checkLoggedin(){
  return new Promise(function(resolve, reject) {
    var UID = getUID();
    var Authtoken = getAuthtoken();
    Promise.all([UID, Authtoken]).then(function(values){
      if(values[0] && values[1]){
        $.ajax({
          url: apiUrl,
          method: "POST",
          data: "action=checklogin&Authtoken=" + values[1] + "&UID=" + values[0],
          success: function(res){
            var data = JSON.parse(res);
            switch(data.status){
              case 200:
                resolve(true);
                break;
              case 403:
                resolve(false);
                break;
              case 500:
                resolve(false);
                break;
              case 666:
                resolve(666);
              default:
                $.log("Something is wrong with the checklogin");
                resolve(false);
                break;
            }
          }
        });
      }else{
        resolve(false);
      }
    });
  });
}


// Function to hide everything when a user isn't logged in
$.hideLoggedOut = function(){
  // Show all the stuff that the user should be able to see
  $("[data-loggedin='false']").show();
  // Hide the stuff that the user shouldn't be able to see
  $("[data-loggedin='true']").hide();
}

// Function to hide everything when a user isn't logged in
$.showLoggedIn = function(){
  // Show all the stuff that the user should be able to see
  $("[data-loggedin='true']").show();
  // Hide the stuff that the user shouldn't be able to see
  $("[data-loggedin='false']").hide();
}


// Function to load pages
$.loadpage = function (page){
  $.showLoader("loader","content-wrapper");

  $.get("pages/"+page+".html",function(){
    $.log("Navigating to: " + page);
    $('#page').load('pages/'+page+'.html', function() {
      changeTitle(pageConfig["title"]).then(function(){
        $.log("Loaded: " + page);
        $.log("Page loaded, hiding loader");
        $.hideLoader("loader","content-wrapper");
      });
    });
  })
  .fail(function(){
    $.log("Could not find page!\r\nNavigating to 404 page");
    $('#page').load('pages/404.html', function() {
      $.log("Loaded: 404");
      changeTitle(pageConfig["title"]).then(function(){
        $.log("Page loaded hiding loader");
        $.hideLoader("loader","content-wrapper");
      });
    });
  });

}

async function changeTitle(title){
  return new Promise(function(resolve, reject) {
    $.log("Editing navbar title to \"" + title + "\"");
    $("#navbar-brand").text(title);
    resolve();
  });
}

var page;
var at;
$(window).on('hashchange', function(e){
  var hash = window.location.hash;
  hash = hash.split("#");
  var tempPage = hash[1];
  if(tempPage.indexOf("@") !== -1){
    data = tempPage.split("@");
    page = data[0];
    at = data[1];
  }else{
    page = tempPage;
  }
  $.loadpage(page);
});

$(function(){
  $("#logout").click(function() {
    $.log("Logging out user");
    var UID = getUID();
    var Authtoken = getAuthtoken();
    Promise.all([UID, Authtoken]).then(function(values){
      $.ajax({
        url: apiUrl,
        method: "POST",
        data: "action=logout&Authtoken=" + values[1] + "&UID=" + values[0]
      });
      // Remove the userdata
      userData.removeItem('UID');
      userData.removeItem('Authtoken');

      // Hide stuff user shouldn't see, and show what they should see
      $.hideLoggedOut();

      // Load homepage
      history.pushState("#home", document.title,"#home");
      $.loadpage("home");
    });
  });

  $("#invite-a-friend").click(function(e) {
    e.preventDefault();
    var message = `Ik heb je uitgenodigt om de gratis MijnLijstjes-Alpha app te gebruiken!%0A
Deze is te downloaden via:%0A
https://play.google.com/store/apps/details?id=nl.finlaydag33k.mijnlijstjes


(Dit is een automatisch gegenereerd bericht op aanvraag van de verstuurder)
    `;
    window.location.href = "whatsapp://send?text="+message;
  });
});
