# Class-FinlayDaG33k
A set of classes in a nice module fashion containing some handy functions that I use on a regular basis.

# Documentation
The full documentation of this library can be found at [FinlayDaG33k Docs](https://docs.finlaydag33k.nl/docs/Class-FinlayDaG33k)
