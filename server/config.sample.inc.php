<?php
$appConfig = array(
  "MySQL" => array(
    "Host"     => "",
    "Username" => "",
    "Password" => "",
    "Database" => ""
  ),
  "SMTP" => array(
    "Host"     => "",
    "Username" => "",
    "Password" => '',
    "Reply-to" => array(
      "Name" => "",
      "Mail" => ""
    )
  )
);

require("propel.config.php");