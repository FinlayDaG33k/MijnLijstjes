<?php
  switch(strtolower($_POST['action'])){
    case "register":
      include("POST/register.php");
      break;
    case "login":
      include("POST/login.php");
      break;
    case "checklogin":
      include("POST/checkLogin.php");
      break;
    case "createlijstje":
      include("POST/createLijstje.php");
      break;
    case "listlijstjes":
      include("POST/listLijstjes.php");
      break;
    case "getlijstje":
      include("POST/getLijstje.php");
      break;
    case "additem":
      include("POST/addItem.php");
      break;
    case "stripeitem":
      include("POST/stripeItem.php");
      break;
    case "deleteitem":
      include("POST/deleteItem.php");
      break;
    case "deletelijstje":
      include("POST/deleteLijstje.php");
      break;
    case "adduser":
      include("POST/addUser.php");
      break;
    case "updatepermissions":
      include("POST/updatePermissions.php");
      break;
    case "deleteuser":
      include("POST/deleteUser.php");
      break;
    case "getusers":
      include("POST/getUsers.php");
      break;
    case "getuserdetails":
      include("POST/getUserdetails.php");
      break;
    case "editaccount":
      include("POST/editAccount.php");
      break;
    case "changepassword":
      include("POST/changePassword.php");
      break;
    case "logout":
      include("POST/logout.php");
      break;
    default:
      $apiOutput = array("status"=>400,"message"=>"Method " . htmlentities($_POST['action']) . " not available in POST");
      break;
  }
