<?php
if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
  $user = AuthtokensQuery::create()
    ->filterByToken($_POST['Authtoken'])
    ->_and()
    ->filterByUID($_POST['UID'])
    ->findOne();
  if($user != null){
    $permissions = LijstjespermsQuery::create()
      ->filterByID($_POST['pid'])
      ->_and()
      ->filterByOID($_POST['UID'])
      ->findOne();
    if($permissions != null){
      switch($_POST['perm']){
        case "E":
          $permissions->setE((int)($_POST['value'] === 'true'));
          break;
        case "A":
          $permissions->setA((int)($_POST['value'] === 'true'));
          break;
        case "D":
          $permissions->setD((int)($_POST['value'] === 'true'));
          break;
        case "S":
          $permissions->setS((int)($_POST['value'] === 'true'));
          break;
      }

      if($permissions->save()){
        $apiOutput = array("status"=>200,"message"=>"Permission updated!");
      }else{
        $apiOutput = array("status"=>500,"message"=>"Could not update permission!");
      }
    }else{
      $apiOutput = array("status"=>404,"message"=>"No lijstje found!");
    }
  }else{
    $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
  }
}else{
  $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
}
