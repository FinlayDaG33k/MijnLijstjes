<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $permissions = LijstjespermsQuery::create()
        ->filterByOID($_POST['UID'])
        ->_or()
        ->filterBySID($_POST['SID'])
        ->_and()
        ->filterByLID($_POST['lid'])
        ->_and()
        ->filterByS(1)
        ->findOne();
      if($permissions != null){
        $lijstje = LijstjesQuery::create()
          ->filterByLID($_POST['lid'])
          ->findOne();
        if($lijstje != null){
          $data = json_decode($lijstje->getData(),1);
          if($_POST['status'] == "true"){
            $status = 1;
          }else{
            $status = 0;
          }
          $data[$_POST['item']]['status'] = $status;
          $lijstje->setData(json_encode($data));
          if($lijstje->save()){
            $apiOutput = array("status"=>200,"message"=>"Item has been updated!");
          }else{
            $apiOutput = array("status"=>500,"message"=>"Could not update Item");
          }
        }else{
          $apiOutput = array("status"=>404,"message"=>"Lijstje not found");
        }
      }else{
        $apiOutput = array("status"=>404,"message"=>"Lijstje not found");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
