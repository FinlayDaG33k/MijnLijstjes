<?php
  $postData = $_POST;
  $filled = array_filter($postData);
  if(count($postData) == count($filled)){
    $user = UsersQuery::create()
      ->filterByUsername($_POST['username'])
      ->_or()
      ->filterByemail($_POST['username'])
      ->findOne();
    if($user != null){
      if(password_verify($_POST['password'],$user->getPassword())){
        $authToken = $FinlayDaG33k->EzServer->randomStr(16);
        $createToken = new Authtokens();
        $createToken->setUID($user->getID());
        $createToken->setToken($authToken);
        if($createToken->save()){
          $apiOutput = array(
            "status"=>200,
            "message"=>"Login!",
            "result"=>array(
              "UID" => $user->getID(),
              "Authtoken" => $authToken
            )
          );
        }else{
          $apiOutput = array("status" => 500, "message" => "Could not create authtoken");
        }
      }else{
        $apiOutput = array("status" => 403, "message" => "Invalid username/password");
      }
    }else{
      $apiOutput = array("status" => 403, "message" => "Invalid username/password");
    }
  }else{
    $empty = array_diff_key($postData, $filled);
    $apiOutput = array("status" => 400, "message" => "Some values have been left blank: " . implode(', ', array_keys($empty)));
  }
