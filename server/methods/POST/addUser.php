<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $permissions = LijstjespermsQuery::create()
        ->filterByLID($_POST['lid'])
        ->_and()
        ->filterByOID($_POST['UID'])
        ->findOne();
      if($permissions != null){
        $user = UsersQuery::create()
          ->filterByUsername($_POST['username'])
          ->_or()
          ->filterByemail($_POST['username'])
          ->findOne();
        if($user != null){
          $permissions = LijstjespermsQuery::create()
            ->filterByLID($_POST['lid'])
            ->_and()
            ->filterByOID($_POST['UID'])
            ->_and()
            ->filterBySID($user->getID())
            ->findOne();
          if($_POST['mayAdd'] == 'on'){
            $mayAdd = 1;
          }else{
            $mayAdd = 0;
          }
          if($_POST['mayEdit'] == 'on'){
            $mayEdit = 1;
          }else{
            $mayEdit = 0;
          }
          if($_POST['mayStripe'] == 'on'){
            $mayStripe = 1;
          }else{
            $mayStripe = 0;
          }
          if($_POST['mayDelete'] == 'on'){
            $mayDelete = 1;
          }else{
            $mayDelete = 0;
          }
          if($permissions != null){
            $permissions->setR(1);
            $permissions->setE($mayEdit);
            $permissions->setA($mayAdd);
            $permissions->setD($mayDelete);
            $permissions->setS($mayStripe);
            if($permissions->save()){
              $apiOutput = array("status"=>200,"message"=>"User updated!");
            }else{
              $apiOutput = array("status"=>500,"message"=>"Could not update user!");
            }
          }else{
            $permissions = new Lijstjesperms();
            $permissions->setOID($_POST['UID']);
            $permissions->setSID($user->getID());
            $permissions->setLID($_POST['lid']);
            $permissions->setR(1);
            $permissions->setE($mayEdit);
            $permissions->setA($mayAdd);
            $permissions->setD($mayDelete);
            $permissions->setS($mayStripe);
            if($permissions->save()){
              $apiOutput = array("status"=>200,"message"=>"User added!");
            }else{
              $apiOutput = array("status"=>500,"message"=>"Could not add user!");
            }
          }
        }else{
          $apiOutput = array("status"=>"404-1","message"=>"User not found!");
        }
      }else{
        $apiOutput = array("status"=>404,"message"=>"You do not own this lijstje!");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
