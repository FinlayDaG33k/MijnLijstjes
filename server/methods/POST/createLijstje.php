<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $lijstje = new Lijstjes();
      $lijstje->setOID($_POST['UID']);
      $lijstje->setName($_POST['lijstjeNaam']);
      $lijstje->setData("[]");
      if($lijstje->save()){
        $permissions = new Lijstjesperms();
        $permissions->setOID($_POST['UID']);
        $permissions->setSID($_POST['UID']);
        $permissions->setLID($lijstje->getLID());
        $permissions->setR(1);
        $permissions->setE(1);
        $permissions->setA(1);
        $permissions->setD(1);
        $permissions->setS(1);
        if($permissions->save()){
          $apiOutput = array("status"=>200,"message"=>"Lijstje created!");
        }else{
          $apiOutput = array("status"=>500,"message"=>"Could not create permissions for Lijstje");
        }
      }else{
        $apiOutput = array("status"=>500,"message"=>"Could not create Lijstje");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
