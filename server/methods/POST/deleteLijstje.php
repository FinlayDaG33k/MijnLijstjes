<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $permissions = LijstjespermsQuery::create()
        ->filterByLID($_POST['lid'])
        ->_and()
        ->filterByOID($_POST['UID'])
        ->find();
      if($permissions != null){
        $lijstje = LijstjesQuery::create()
          ->filterByLID($_POST['lid'])
          ->_and()
          ->filterByOID($_POST['UID'])
          ->delete();
        if($lijstje){
          $apiOutput = array("status"=>200,"message"=>"Lijstje deleted!");
        }else{
          $apiOutput = array("status"=>500,"message"=>"Could not delete Lijstje!");
        }
      }else{
        $permissions = LijstjespermsQuery::create()
          ->filterByLID($_POST['lid'])
          ->_and()
          ->filterBySID($_POST['UID'])
          ->findOne();
        if($permissions != null){
          $permissions->delete();
          if($permissions->isDeleted()){
            $apiOutput = array("status"=>200,"message"=>"User removed from Lijstje!");
          }else{
            $apiOutput = array("status"=>500,"message"=>"Could not remove user from Lijstje!");
          }
        }else{
          $apiOutput = array("status"=>404,"message"=>"No lijstje found!");
        }
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
