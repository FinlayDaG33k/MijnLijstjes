<?php
if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
  $user = AuthtokensQuery::create()
    ->filterByToken($_POST['Authtoken'])
    ->_and()
    ->filterByUID($_POST['UID'])
    ->findOne();
  if($user != null){
    $permissions = LijstjespermsQuery::create()
      ->filterByLID($_POST['lid'])
      ->_and()
      ->filterByOID($_POST['UID'])
      ->find();
    if($permissions != null){
      $firstRow = true;
      $sql = "";
      $permissionData = array();
      foreach($permissions as $permission){
        $user = UsersQuery::create()
          ->filterByID($permission->getSID())
          ->findOne();
        $permissionData[] = array("Username"=>$user->getUsername(),"OID"=>$permission->getOID(),"SID"=>$permission->getSID(),"ID"=>$permission->getID(),"R"=>$permission->getR(),"E"=>$permission->getE(),"A"=>$permission->getA(),"D"=>$permission->getD(),"S"=>$permission->getS());
      }
      $apiOutput = array("status"=>200,"message"=>"Permissions loaded!","result"=>json_encode($permissionData));
    }else{
      $apiOutput = array("status"=>404,"message"=>"User does not own lijstje!");
    }
  }else{
    $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
  }
}else{
  $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
}
