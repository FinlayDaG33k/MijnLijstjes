<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $delete = AuthtokensQuery::create()
        ->filterByToken($_POST['Authtoken'])
        ->filterByUID($_POST['UID'])
        ->delete();
      if($delete){
        $apiOutput = array("status"=>200,"message"=>"Logout success!");
      }else{
        $apiOutput = array("status"=>500,"message"=>"Could not securely logout!");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Invalid authtoken/UID combination");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
