<?php
  $postData = $_POST;
  $filled = array_filter($postData);
  if(count($postData) == count($filled)){
    $user = UsersQuery::create()
      ->filterByUsername($_POST['username'])
      ->_or()
      ->filterByEmail($_POST['email'])
      ->findOne();
    if($user == null){
      $user = new Users();
      $user->setVoornaam($_POST['first_name']);
      $user->setAchternaam($_POST['last_name']);
      $user->setUsername($_POST['username']);
      $user->setEmail($_POST['email']);
      $user->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
      if($user->save()){
        $authToken = $FinlayDaG33k->EzServer->randomStr(16);
        $UID = $user->getId();

        $createToken = new Authtokens();
        $createToken->setUID($UID);
        $createToken->setToken($authToken);
        if($createToken->save()){
          $activationToken = $FinlayDaG33k->EzServer->randomStr(8);
          $createActivationToken = new Activationtokens();
          $createActivationToken->setUID($UID);
          $createActivationToken->setToken($activationToken);
          if($createActivationToken->save()){
            try {
              $mail = initializeMail($appConfig['SMTP']);
              //Content
              $mail->isHTML(true); // Set email format to HTML
              $mail->Subject = "Activeer je MijnLijstjes account!";
              $activationURL = $FinlayDaG33k->EzServer->getHome() . "/?action=activateAccount&token=".$activationToken;
              $mail->Body    = createMail("Beste ".$_POST['first_name'].",<br />Activeer je MijnLijstjes account door op de volgende link te klikken:<br /><a href=\"".$activationURL."\">".$activationURL."</a>");
              $mail->AltBody = "Activeer je MijnLijstjes account door op de volgende link te klikken:\r\n".$FinlayDaG33k->EzServer->getHome() . "/?action=activateAccount&token=".$activationToken;

              $mail->send();
              $apiOutput = array(
                "status"=>200,
                "message"=>"Registration successful!",
                "result"=>array(
                  "Username" => $_POST['username'],
                  "Email" => $_POST['email'],
                  "UID" => $UID,
                  "Authtoken" => $authToken
                )
              );
            } catch (Exception $e) {
              $apiOutput = array("status" => 200, "message" => "Activation mail could not be send!", "error" => $mail->ErrorInfo);
            }
          }else{
            $apiOutput = array(
              "status"=>200,
              "message"=>"Registration successful!",
              "result"=>array(
                "Username" => $_POST['username'],
                "Email" => $_POST['email']
              )
            );
          }
        }else{
          $apiOutput = array(
            "status"=>200,
            "message"=>"Registration successful!",
            "result"=>array(
              "Username" => $_POST['username'],
              "Email" => $_POST['email']
            )
          );
        }
      }else{
        $apiOutput = array("status"=>500,"message"=>"Could not create new user");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Username or Email already taken!");
    }
  }else{
    $empty = array_diff_key($postData, $filled);
    $apiOutput = array("status" => 400, "message" => "Some values have been left blank: " . implode(', ', array_keys($empty)));
  }
