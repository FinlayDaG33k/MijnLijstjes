<?php
if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
  $user = AuthtokensQuery::create()
    ->filterByToken($_POST['Authtoken'])
    ->_and()
    ->filterByUID($_POST['UID'])
    ->findOne();
  if($user != null){
    $permissions = LijstjespermsQuery::create()
      ->filterByID($_POST['pid'])
      ->_and()
      ->filterByOID($_POST['UID'])
      ->findOne();
    if($permissions != null){
      $permissions->delete();
      if($permissions->isDeleted()){
        $apiOutput = array("status"=>200,"message"=>"User removed!");
      }else{
        $apiOutput = array("status"=>500,"message"=>"Could not remove user!");
      }
    }else{
      $apiOutput = array("status"=>404,"message"=>"No lijstje found!");
    }
  }else{
    $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
  }
}else{
  $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
}
