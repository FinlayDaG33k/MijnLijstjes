<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $permissions = LijstjespermsQuery::create()
        ->filterByOID($_POST['UID'])
        ->_or()
        ->filterBySID($_POST['UID'])
        ->_and()
        ->filterByR('1')
        ->find();
      if($permissions != null){
        $lijstje = LijstjesQuery::create()
          ->filterByLID($_POST['lid'])
          ->findOne();
        $lijstjeData = array(
          "Owner" => $lijstje->getOID(),
          "Name" => $lijstje->getName(),
          "Data" => $lijstje->getData()
        );
        if(!empty($lijstjeData)){
          $apiOutput = array("status"=>200,"message"=>"Lijstje loaded!", "result"=>$lijstjeData);
        }else{
          $apiOutput = array("status"=>500,"message"=>"Could not load Lijstje!");
        }
      }else{
        $apiOutput = array("status"=>404,"message"=>"No lijstje found!");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
