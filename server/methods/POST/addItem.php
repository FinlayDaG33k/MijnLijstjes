<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $permissions = LijstjespermsQuery::create()
        ->filterByLID($_POST['lid'])
        ->_and()
        ->filterByOID($_POST['UID'])
        ->_or()
        ->filterBySID($_POST['UID'])
        ->_and()
        ->filterByA('1')
        ->find();
      if($permissions != null){
        $lijstje = LijstjesQuery::create()
          ->filterByLID($_POST['lid'])
          ->findOne();
        if($lijstje != null){
          $data = json_decode($lijstje->getData(),1);
          $user = UsersQuery::create()
            ->filterByID($_POST['UID'])
            ->findOne();
          if($user != null){
            $data[] = array(
              "Name" => $_POST['itemName'],
              "notes" => $_POST['itemNote'],
              "Added" => array(
                "By" => $user->getUsername(),
                "On" => time()
              ),
              "status" => 0
            );
            $lijstje->setData(json_encode($data));
            if($lijstje->save()){
              $apiOutput = array("status"=>200,"message"=>"Lijstje has been updated!");
            }else{
              $apiOutput = array("status"=>500,"message"=>"Could not update Lijstje");
            }
          }else{
            $apiOutput = array("status"=>500,"message"=>"Could not get username of user");
          }
        }else{
          $apiOutput = array("status"=>404,"message"=>"Lijstje not found");
        }
      }else{
        $apiOutput = array("status"=>404,"message"=>"Lijstje not found");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
