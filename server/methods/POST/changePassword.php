<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $user = UsersQuery::create()
        ->filterByID($_POST['UID'])
        ->findOne();
      if($user != null){
        if(password_verify($_POST['password_current'],$user->getPassword())){
          $user->setPassword(password_hash($_POST['password_new'],PASSWORD_DEFAULT));
          if($user->save()){
            $apiOutput = array("status"=>200,"message"=>"Update success!");
          }else{
            $apiOutput = array("status"=>500,"message"=>"Could not update password!");
          }
        }else{
          $apiOutput = array("status"=>403,"message"=>"Invalid password!");
        }
      }else{
        $apiOutput = array("status"=>500,"message"=>"Could not load userdetails");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
