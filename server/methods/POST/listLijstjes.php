<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $permissions = LijstjespermsQuery::create()
        ->filterBySID($_POST['UID'])
        ->_and()
        ->filterByR('1')
        ->find();
      if($permissions != null){
        $lijstjeData = array();
        foreach($permissions as $permission){
          $lijstje = LijstjesQuery::create()
            ->filterByLID($permission->getLID())
            ->findOne();
          $owner = UsersQuery::create()
            ->filterByID($permission->getOID())
            ->findOne();
          $lijstjeData[] = array(
            "LID"=>$permission->getLID(),
            "Name"=>$lijstje->getName(),
            "Owner"=>$owner->getUsername()
          );
        }
        $apiOutput = array("status"=>200,"message"=>"Loading Lijstjes successfully!", "result"=>$lijstjeData);
      }else{
        $apiOutput = array("status"=>404,"message"=>"No lijstjes found!");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
