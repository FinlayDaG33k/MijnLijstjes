<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $user = UsersQuery::create()
        ->filterByID($_POST['UID'])
        ->findOne();
      if($user != null){
        $data = array("Voornaam"=>$user->getVoornaam(),"Achternaam"=>$user->getAchternaam(),"Username"=>$user->getUsername(),"email"=>$user->getemail());
        $apiOutput = array("status"=>200,"message"=>"Fetched userdetails", "result" => json_encode($data));
      }else{
        $apiOutput = array("status"=>500,"message"=>"Could not load userdetails!");
      }
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
