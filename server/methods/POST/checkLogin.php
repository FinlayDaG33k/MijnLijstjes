<?php
  if(!empty($_POST['Authtoken']) && !empty($_POST['UID'])){
    $user = AuthtokensQuery::create()
      ->filterByToken($_POST['Authtoken'])
      ->_and()
      ->filterByUID($_POST['UID'])
      ->findOne();
    if($user != null){
      $apiOutput = array("status"=>200,"message"=>"Authtoken Valid!");
    }else{
      $apiOutput = array("status"=>403,"message"=>"Authtoken invalid!");
    }
  }else{
    $apiOutput = array("status"=>400,"message"=>"Authtoken and UID cannot be left empty!");
  }
