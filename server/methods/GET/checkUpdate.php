<?php
  $dir = $_SERVER['DOCUMENT_ROOT']."/dl";
  if (is_dir($dir)) {
    $files = scandir($dir, SCANDIR_SORT_DESCENDING);
    $newestBin = $files[0];
    $latestVersion = strrev(explode(".",strrev(explode("-",$newestBin,2)[1]),2)[1]);
    $apiOutput = array(
      "status" => 200,
      "message" => "Lastest update found!",
      "result"=>array(
        "isNewer"=>version_compare($latestVersion, $_GET['currentversion'], '>'),
        "version"=>$latestVersion,
        "fileName" => $newestBin,
        "url"=>$FinlayDaG33k->EzServer->getRoot()."/dl/".$newestBin,
        "sha1"=>sha1_file("dl/".$newestBin)
      )
    );
  }else{
    $apiOutput = array("status" => 500, "message" => "Could not load downloads directory!");
  }
