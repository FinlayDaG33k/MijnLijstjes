<?php
  $token = ActivationtokensQuery::create()
    ->filterByToken($_GET['token'])
    ->findOne();
  if($token != null){
    $user = UsersQuery::create()
      ->filterByID($token->getUID())
      ->findOne();
    if($user != null){
      $user->setIsActivated('1');
      if($user->save()){
        $token->delete();
        die("Account is geactiveerd!");
      }else{
        die("Kon account niet activeren");
      }
    }else{
      die("Ongeldige activatie token!");
    }
  }else{
    die("Ongeldige activatie token!");
  }
