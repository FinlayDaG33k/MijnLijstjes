<?php
  switch(strtolower($_GET['action'])){
    case "checkupdate":
      include("GET/checkUpdate.php");
      break;
    case "activateaccount":
      include("GET/activateAccount.php");
      break;
    default:
      $apiOutput = array("status"=>400,"message"=>"Method " . htmlentities($_GET['action']) . " not available in GET");
      break;
  }
