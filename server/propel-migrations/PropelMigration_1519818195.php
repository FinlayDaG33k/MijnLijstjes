<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1519818195.
 * Generated on 2018-02-28 11:43:15
 */
class PropelMigration_1519818195
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `Authtokens`

  CHANGE `UID` `UID` INTEGER;

CREATE INDEX `Authtokens_fi_e6ebd1` ON `Authtokens` (`UID`);

ALTER TABLE `Authtokens` ADD CONSTRAINT `Authtokens_fk_e6ebd1`
    FOREIGN KEY (`UID`)
    REFERENCES `Users` (`ID`);

ALTER TABLE `Lijstjes`

  CHANGE `OID` `OID` INTEGER;

CREATE INDEX `Lijstjes_fi_e82be4` ON `Lijstjes` (`OID`);

ALTER TABLE `Lijstjes` ADD CONSTRAINT `Lijstjes_fk_e82be4`
    FOREIGN KEY (`OID`)
    REFERENCES `Users` (`ID`);

ALTER TABLE `LijstjesPerms`

  CHANGE `OID` `OID` INTEGER,

  CHANGE `SID` `SID` INTEGER,

  CHANGE `LID` `LID` INTEGER;

CREATE INDEX `fi_2oqt9m99xx` ON `LijstjesPerms` (`OID`);

CREATE INDEX `fi_b29yteg5q8` ON `LijstjesPerms` (`SID`);

CREATE INDEX `LijstjesPerms_fi_d63d6c` ON `LijstjesPerms` (`LID`);

ALTER TABLE `LijstjesPerms` ADD CONSTRAINT `fk_2oqt9m99xx`
    FOREIGN KEY (`OID`)
    REFERENCES `Users` (`ID`);

ALTER TABLE `LijstjesPerms` ADD CONSTRAINT `fk_b29yteg5q8`
    FOREIGN KEY (`SID`)
    REFERENCES `Users` (`ID`);

ALTER TABLE `LijstjesPerms` ADD CONSTRAINT `LijstjesPerms_fk_d63d6c`
    FOREIGN KEY (`LID`)
    REFERENCES `Lijstjes` (`LID`);

ALTER TABLE `activationTokens`

  CHANGE `UID` `UID` INTEGER;

CREATE INDEX `activationTokens_fi_e6ebd1` ON `activationTokens` (`UID`);

ALTER TABLE `activationTokens` ADD CONSTRAINT `activationTokens_fk_e6ebd1`
    FOREIGN KEY (`UID`)
    REFERENCES `Users` (`ID`);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `Authtokens` DROP FOREIGN KEY `Authtokens_fk_e6ebd1`;

DROP INDEX `Authtokens_fi_e6ebd1` ON `Authtokens`;

ALTER TABLE `Authtokens`

  CHANGE `UID` `UID` INTEGER NOT NULL;

ALTER TABLE `Lijstjes` DROP FOREIGN KEY `Lijstjes_fk_e82be4`;

DROP INDEX `Lijstjes_fi_e82be4` ON `Lijstjes`;

ALTER TABLE `Lijstjes`

  CHANGE `OID` `OID` INTEGER NOT NULL;

ALTER TABLE `LijstjesPerms` DROP FOREIGN KEY `fk_2oqt9m99xx`;

ALTER TABLE `LijstjesPerms` DROP FOREIGN KEY `fk_b29yteg5q8`;

ALTER TABLE `LijstjesPerms` DROP FOREIGN KEY `LijstjesPerms_fk_d63d6c`;

DROP INDEX `fi_2oqt9m99xx` ON `LijstjesPerms`;

DROP INDEX `fi_b29yteg5q8` ON `LijstjesPerms`;

DROP INDEX `LijstjesPerms_fi_d63d6c` ON `LijstjesPerms`;

ALTER TABLE `LijstjesPerms`

  CHANGE `OID` `OID` INTEGER NOT NULL,

  CHANGE `SID` `SID` INTEGER NOT NULL,

  CHANGE `LID` `LID` INTEGER NOT NULL;

ALTER TABLE `activationTokens` DROP FOREIGN KEY `activationTokens_fk_e6ebd1`;

DROP INDEX `activationTokens_fi_e6ebd1` ON `activationTokens`;

ALTER TABLE `activationTokens`

  CHANGE `UID` `UID` INTEGER(255) NOT NULL;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}
