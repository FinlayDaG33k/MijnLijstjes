<?php
// Load PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function initializeMail($appConfig){
  $mail = new PHPMailer(true); // create new instance
  //Server settings
  $mail->SMTPDebug = 0;                                 // Disable debug output
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = $appConfig['Host']; // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = $appConfig['Username'];                 // SMTP username
  $mail->Password = $appConfig['Password'];                            // SMTP password
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to

  // Who needs valid certs anyways?
  $mail->SMTPOptions = array(
      'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
      )
  );

  //Recipients
  $mail->setFrom($appConfig['Username'], 'MijnLijstjes');
  $mail->addAddress($_POST['email'], $_POST['first_name'] . " " . $_POST['last_name']);     // Add a recipient
  if(!empty($appConfig['Reply-to']['Name']) && !empty($appConfig['Reply-to']['Mail'])){
    $mail->addReplyTo($appConfig['Reply-to']['Mail'], $appConfig['Reply-to']['Name']);
  }
  return $mail;
}

function createMail($body){
  $mailTemplate = file_get_contents("mailtemplates/index.php");
  $mail = str_replace("{{%body%}}",$body,$mailTemplate);
  return $mail;
}
