<?php
  header('Access-Control-Allow-Origin: *'); // enable CORS
  if(file_exists("maintenance.lock")){
    echo json_encode(
      array(
        "status" => 666,
        "message" => "Server is in maintenance mode!"
      )
    );
    exit();
  }
  include("api.php");
