<?php

use Base\LijstjespermsQuery as BaseLijstjespermsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'LijstjesPerms' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LijstjespermsQuery extends BaseLijstjespermsQuery
{

}
