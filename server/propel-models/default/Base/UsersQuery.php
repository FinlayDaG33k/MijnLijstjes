<?php

namespace Base;

use \Users as ChildUsers;
use \UsersQuery as ChildUsersQuery;
use \Exception;
use \PDO;
use Map\UsersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'Users' table.
 *
 *
 *
 * @method     ChildUsersQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     ChildUsersQuery orderByVoornaam($order = Criteria::ASC) Order by the Voornaam column
 * @method     ChildUsersQuery orderByAchternaam($order = Criteria::ASC) Order by the Achternaam column
 * @method     ChildUsersQuery orderByUsername($order = Criteria::ASC) Order by the Username column
 * @method     ChildUsersQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildUsersQuery orderByPassword($order = Criteria::ASC) Order by the Password column
 * @method     ChildUsersQuery orderByIsactivated($order = Criteria::ASC) Order by the isActivated column
 *
 * @method     ChildUsersQuery groupById() Group by the ID column
 * @method     ChildUsersQuery groupByVoornaam() Group by the Voornaam column
 * @method     ChildUsersQuery groupByAchternaam() Group by the Achternaam column
 * @method     ChildUsersQuery groupByUsername() Group by the Username column
 * @method     ChildUsersQuery groupByEmail() Group by the email column
 * @method     ChildUsersQuery groupByPassword() Group by the Password column
 * @method     ChildUsersQuery groupByIsactivated() Group by the isActivated column
 *
 * @method     ChildUsersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsersQuery leftJoinAuthtokens($relationAlias = null) Adds a LEFT JOIN clause to the query using the Authtokens relation
 * @method     ChildUsersQuery rightJoinAuthtokens($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Authtokens relation
 * @method     ChildUsersQuery innerJoinAuthtokens($relationAlias = null) Adds a INNER JOIN clause to the query using the Authtokens relation
 *
 * @method     ChildUsersQuery joinWithAuthtokens($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Authtokens relation
 *
 * @method     ChildUsersQuery leftJoinWithAuthtokens() Adds a LEFT JOIN clause and with to the query using the Authtokens relation
 * @method     ChildUsersQuery rightJoinWithAuthtokens() Adds a RIGHT JOIN clause and with to the query using the Authtokens relation
 * @method     ChildUsersQuery innerJoinWithAuthtokens() Adds a INNER JOIN clause and with to the query using the Authtokens relation
 *
 * @method     ChildUsersQuery leftJoinLijstjes($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lijstjes relation
 * @method     ChildUsersQuery rightJoinLijstjes($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lijstjes relation
 * @method     ChildUsersQuery innerJoinLijstjes($relationAlias = null) Adds a INNER JOIN clause to the query using the Lijstjes relation
 *
 * @method     ChildUsersQuery joinWithLijstjes($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lijstjes relation
 *
 * @method     ChildUsersQuery leftJoinWithLijstjes() Adds a LEFT JOIN clause and with to the query using the Lijstjes relation
 * @method     ChildUsersQuery rightJoinWithLijstjes() Adds a RIGHT JOIN clause and with to the query using the Lijstjes relation
 * @method     ChildUsersQuery innerJoinWithLijstjes() Adds a INNER JOIN clause and with to the query using the Lijstjes relation
 *
 * @method     ChildUsersQuery leftJoinLijstjespermsRelatedByOid($relationAlias = null) Adds a LEFT JOIN clause to the query using the LijstjespermsRelatedByOid relation
 * @method     ChildUsersQuery rightJoinLijstjespermsRelatedByOid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LijstjespermsRelatedByOid relation
 * @method     ChildUsersQuery innerJoinLijstjespermsRelatedByOid($relationAlias = null) Adds a INNER JOIN clause to the query using the LijstjespermsRelatedByOid relation
 *
 * @method     ChildUsersQuery joinWithLijstjespermsRelatedByOid($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LijstjespermsRelatedByOid relation
 *
 * @method     ChildUsersQuery leftJoinWithLijstjespermsRelatedByOid() Adds a LEFT JOIN clause and with to the query using the LijstjespermsRelatedByOid relation
 * @method     ChildUsersQuery rightJoinWithLijstjespermsRelatedByOid() Adds a RIGHT JOIN clause and with to the query using the LijstjespermsRelatedByOid relation
 * @method     ChildUsersQuery innerJoinWithLijstjespermsRelatedByOid() Adds a INNER JOIN clause and with to the query using the LijstjespermsRelatedByOid relation
 *
 * @method     ChildUsersQuery leftJoinLijstjespermsRelatedBySid($relationAlias = null) Adds a LEFT JOIN clause to the query using the LijstjespermsRelatedBySid relation
 * @method     ChildUsersQuery rightJoinLijstjespermsRelatedBySid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LijstjespermsRelatedBySid relation
 * @method     ChildUsersQuery innerJoinLijstjespermsRelatedBySid($relationAlias = null) Adds a INNER JOIN clause to the query using the LijstjespermsRelatedBySid relation
 *
 * @method     ChildUsersQuery joinWithLijstjespermsRelatedBySid($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LijstjespermsRelatedBySid relation
 *
 * @method     ChildUsersQuery leftJoinWithLijstjespermsRelatedBySid() Adds a LEFT JOIN clause and with to the query using the LijstjespermsRelatedBySid relation
 * @method     ChildUsersQuery rightJoinWithLijstjespermsRelatedBySid() Adds a RIGHT JOIN clause and with to the query using the LijstjespermsRelatedBySid relation
 * @method     ChildUsersQuery innerJoinWithLijstjespermsRelatedBySid() Adds a INNER JOIN clause and with to the query using the LijstjespermsRelatedBySid relation
 *
 * @method     ChildUsersQuery leftJoinActivationtokens($relationAlias = null) Adds a LEFT JOIN clause to the query using the Activationtokens relation
 * @method     ChildUsersQuery rightJoinActivationtokens($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Activationtokens relation
 * @method     ChildUsersQuery innerJoinActivationtokens($relationAlias = null) Adds a INNER JOIN clause to the query using the Activationtokens relation
 *
 * @method     ChildUsersQuery joinWithActivationtokens($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Activationtokens relation
 *
 * @method     ChildUsersQuery leftJoinWithActivationtokens() Adds a LEFT JOIN clause and with to the query using the Activationtokens relation
 * @method     ChildUsersQuery rightJoinWithActivationtokens() Adds a RIGHT JOIN clause and with to the query using the Activationtokens relation
 * @method     ChildUsersQuery innerJoinWithActivationtokens() Adds a INNER JOIN clause and with to the query using the Activationtokens relation
 *
 * @method     \AuthtokensQuery|\LijstjesQuery|\LijstjespermsQuery|\ActivationtokensQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsers findOne(ConnectionInterface $con = null) Return the first ChildUsers matching the query
 * @method     ChildUsers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsers matching the query, or a new ChildUsers object populated from the query conditions when no match is found
 *
 * @method     ChildUsers findOneById(int $ID) Return the first ChildUsers filtered by the ID column
 * @method     ChildUsers findOneByVoornaam(string $Voornaam) Return the first ChildUsers filtered by the Voornaam column
 * @method     ChildUsers findOneByAchternaam(string $Achternaam) Return the first ChildUsers filtered by the Achternaam column
 * @method     ChildUsers findOneByUsername(string $Username) Return the first ChildUsers filtered by the Username column
 * @method     ChildUsers findOneByEmail(string $email) Return the first ChildUsers filtered by the email column
 * @method     ChildUsers findOneByPassword(string $Password) Return the first ChildUsers filtered by the Password column
 * @method     ChildUsers findOneByIsactivated(int $isActivated) Return the first ChildUsers filtered by the isActivated column *

 * @method     ChildUsers requirePk($key, ConnectionInterface $con = null) Return the ChildUsers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOne(ConnectionInterface $con = null) Return the first ChildUsers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsers requireOneById(int $ID) Return the first ChildUsers filtered by the ID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByVoornaam(string $Voornaam) Return the first ChildUsers filtered by the Voornaam column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByAchternaam(string $Achternaam) Return the first ChildUsers filtered by the Achternaam column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByUsername(string $Username) Return the first ChildUsers filtered by the Username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByEmail(string $email) Return the first ChildUsers filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByPassword(string $Password) Return the first ChildUsers filtered by the Password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByIsactivated(int $isActivated) Return the first ChildUsers filtered by the isActivated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsers objects based on current ModelCriteria
 * @method     ChildUsers[]|ObjectCollection findById(int $ID) Return ChildUsers objects filtered by the ID column
 * @method     ChildUsers[]|ObjectCollection findByVoornaam(string $Voornaam) Return ChildUsers objects filtered by the Voornaam column
 * @method     ChildUsers[]|ObjectCollection findByAchternaam(string $Achternaam) Return ChildUsers objects filtered by the Achternaam column
 * @method     ChildUsers[]|ObjectCollection findByUsername(string $Username) Return ChildUsers objects filtered by the Username column
 * @method     ChildUsers[]|ObjectCollection findByEmail(string $email) Return ChildUsers objects filtered by the email column
 * @method     ChildUsers[]|ObjectCollection findByPassword(string $Password) Return ChildUsers objects filtered by the Password column
 * @method     ChildUsers[]|ObjectCollection findByIsactivated(int $isActivated) Return ChildUsers objects filtered by the isActivated column
 * @method     ChildUsers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UsersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Users', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsersQuery) {
            return $criteria;
        }
        $query = new ChildUsersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT ID, Voornaam, Achternaam, Username, email, Password, isActivated FROM Users WHERE ID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsers $obj */
            $obj = new ChildUsers();
            $obj->hydrate($row);
            UsersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UsersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UsersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the Voornaam column
     *
     * Example usage:
     * <code>
     * $query->filterByVoornaam('fooValue');   // WHERE Voornaam = 'fooValue'
     * $query->filterByVoornaam('%fooValue%', Criteria::LIKE); // WHERE Voornaam LIKE '%fooValue%'
     * </code>
     *
     * @param     string $voornaam The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByVoornaam($voornaam = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($voornaam)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_VOORNAAM, $voornaam, $comparison);
    }

    /**
     * Filter the query on the Achternaam column
     *
     * Example usage:
     * <code>
     * $query->filterByAchternaam('fooValue');   // WHERE Achternaam = 'fooValue'
     * $query->filterByAchternaam('%fooValue%', Criteria::LIKE); // WHERE Achternaam LIKE '%fooValue%'
     * </code>
     *
     * @param     string $achternaam The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByAchternaam($achternaam = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($achternaam)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_ACHTERNAAM, $achternaam, $comparison);
    }

    /**
     * Filter the query on the Username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE Username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE Username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the Password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE Password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE Password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the isActivated column
     *
     * Example usage:
     * <code>
     * $query->filterByIsactivated(1234); // WHERE isActivated = 1234
     * $query->filterByIsactivated(array(12, 34)); // WHERE isActivated IN (12, 34)
     * $query->filterByIsactivated(array('min' => 12)); // WHERE isActivated > 12
     * </code>
     *
     * @param     mixed $isactivated The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByIsactivated($isactivated = null, $comparison = null)
    {
        if (is_array($isactivated)) {
            $useMinMax = false;
            if (isset($isactivated['min'])) {
                $this->addUsingAlias(UsersTableMap::COL_ISACTIVATED, $isactivated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isactivated['max'])) {
                $this->addUsingAlias(UsersTableMap::COL_ISACTIVATED, $isactivated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_ISACTIVATED, $isactivated, $comparison);
    }

    /**
     * Filter the query by a related \Authtokens object
     *
     * @param \Authtokens|ObjectCollection $authtokens the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByAuthtokens($authtokens, $comparison = null)
    {
        if ($authtokens instanceof \Authtokens) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $authtokens->getUid(), $comparison);
        } elseif ($authtokens instanceof ObjectCollection) {
            return $this
                ->useAuthtokensQuery()
                ->filterByPrimaryKeys($authtokens->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAuthtokens() only accepts arguments of type \Authtokens or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Authtokens relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinAuthtokens($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Authtokens');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Authtokens');
        }

        return $this;
    }

    /**
     * Use the Authtokens relation Authtokens object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AuthtokensQuery A secondary query class using the current class as primary query
     */
    public function useAuthtokensQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAuthtokens($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Authtokens', '\AuthtokensQuery');
    }

    /**
     * Filter the query by a related \Lijstjes object
     *
     * @param \Lijstjes|ObjectCollection $lijstjes the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByLijstjes($lijstjes, $comparison = null)
    {
        if ($lijstjes instanceof \Lijstjes) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $lijstjes->getOid(), $comparison);
        } elseif ($lijstjes instanceof ObjectCollection) {
            return $this
                ->useLijstjesQuery()
                ->filterByPrimaryKeys($lijstjes->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLijstjes() only accepts arguments of type \Lijstjes or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lijstjes relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinLijstjes($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lijstjes');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lijstjes');
        }

        return $this;
    }

    /**
     * Use the Lijstjes relation Lijstjes object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LijstjesQuery A secondary query class using the current class as primary query
     */
    public function useLijstjesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLijstjes($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lijstjes', '\LijstjesQuery');
    }

    /**
     * Filter the query by a related \Lijstjesperms object
     *
     * @param \Lijstjesperms|ObjectCollection $lijstjesperms the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByLijstjespermsRelatedByOid($lijstjesperms, $comparison = null)
    {
        if ($lijstjesperms instanceof \Lijstjesperms) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $lijstjesperms->getOid(), $comparison);
        } elseif ($lijstjesperms instanceof ObjectCollection) {
            return $this
                ->useLijstjespermsRelatedByOidQuery()
                ->filterByPrimaryKeys($lijstjesperms->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLijstjespermsRelatedByOid() only accepts arguments of type \Lijstjesperms or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LijstjespermsRelatedByOid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinLijstjespermsRelatedByOid($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LijstjespermsRelatedByOid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LijstjespermsRelatedByOid');
        }

        return $this;
    }

    /**
     * Use the LijstjespermsRelatedByOid relation Lijstjesperms object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LijstjespermsQuery A secondary query class using the current class as primary query
     */
    public function useLijstjespermsRelatedByOidQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLijstjespermsRelatedByOid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LijstjespermsRelatedByOid', '\LijstjespermsQuery');
    }

    /**
     * Filter the query by a related \Lijstjesperms object
     *
     * @param \Lijstjesperms|ObjectCollection $lijstjesperms the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByLijstjespermsRelatedBySid($lijstjesperms, $comparison = null)
    {
        if ($lijstjesperms instanceof \Lijstjesperms) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $lijstjesperms->getSid(), $comparison);
        } elseif ($lijstjesperms instanceof ObjectCollection) {
            return $this
                ->useLijstjespermsRelatedBySidQuery()
                ->filterByPrimaryKeys($lijstjesperms->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLijstjespermsRelatedBySid() only accepts arguments of type \Lijstjesperms or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LijstjespermsRelatedBySid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinLijstjespermsRelatedBySid($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LijstjespermsRelatedBySid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LijstjespermsRelatedBySid');
        }

        return $this;
    }

    /**
     * Use the LijstjespermsRelatedBySid relation Lijstjesperms object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LijstjespermsQuery A secondary query class using the current class as primary query
     */
    public function useLijstjespermsRelatedBySidQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLijstjespermsRelatedBySid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LijstjespermsRelatedBySid', '\LijstjespermsQuery');
    }

    /**
     * Filter the query by a related \Activationtokens object
     *
     * @param \Activationtokens|ObjectCollection $activationtokens the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByActivationtokens($activationtokens, $comparison = null)
    {
        if ($activationtokens instanceof \Activationtokens) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $activationtokens->getUid(), $comparison);
        } elseif ($activationtokens instanceof ObjectCollection) {
            return $this
                ->useActivationtokensQuery()
                ->filterByPrimaryKeys($activationtokens->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByActivationtokens() only accepts arguments of type \Activationtokens or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Activationtokens relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinActivationtokens($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Activationtokens');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Activationtokens');
        }

        return $this;
    }

    /**
     * Use the Activationtokens relation Activationtokens object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ActivationtokensQuery A secondary query class using the current class as primary query
     */
    public function useActivationtokensQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinActivationtokens($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Activationtokens', '\ActivationtokensQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsers $users Object to remove from the list of results
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function prune($users = null)
    {
        if ($users) {
            $this->addUsingAlias(UsersTableMap::COL_ID, $users->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsersTableMap::clearInstancePool();
            UsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsersQuery
