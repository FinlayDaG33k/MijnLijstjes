<?php

namespace Base;

use \Lijstjesperms as ChildLijstjesperms;
use \LijstjespermsQuery as ChildLijstjespermsQuery;
use \Exception;
use \PDO;
use Map\LijstjespermsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'LijstjesPerms' table.
 *
 *
 *
 * @method     ChildLijstjespermsQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     ChildLijstjespermsQuery orderByR($order = Criteria::ASC) Order by the R column
 * @method     ChildLijstjespermsQuery orderByA($order = Criteria::ASC) Order by the A column
 * @method     ChildLijstjespermsQuery orderByE($order = Criteria::ASC) Order by the E column
 * @method     ChildLijstjespermsQuery orderByS($order = Criteria::ASC) Order by the S column
 * @method     ChildLijstjespermsQuery orderByD($order = Criteria::ASC) Order by the D column
 * @method     ChildLijstjespermsQuery orderByOid($order = Criteria::ASC) Order by the OID column
 * @method     ChildLijstjespermsQuery orderBySid($order = Criteria::ASC) Order by the SID column
 * @method     ChildLijstjespermsQuery orderByLid($order = Criteria::ASC) Order by the LID column
 *
 * @method     ChildLijstjespermsQuery groupById() Group by the ID column
 * @method     ChildLijstjespermsQuery groupByR() Group by the R column
 * @method     ChildLijstjespermsQuery groupByA() Group by the A column
 * @method     ChildLijstjespermsQuery groupByE() Group by the E column
 * @method     ChildLijstjespermsQuery groupByS() Group by the S column
 * @method     ChildLijstjespermsQuery groupByD() Group by the D column
 * @method     ChildLijstjespermsQuery groupByOid() Group by the OID column
 * @method     ChildLijstjespermsQuery groupBySid() Group by the SID column
 * @method     ChildLijstjespermsQuery groupByLid() Group by the LID column
 *
 * @method     ChildLijstjespermsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLijstjespermsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLijstjespermsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLijstjespermsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLijstjespermsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLijstjespermsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLijstjespermsQuery leftJoinUsersRelatedByOid($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByOid relation
 * @method     ChildLijstjespermsQuery rightJoinUsersRelatedByOid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByOid relation
 * @method     ChildLijstjespermsQuery innerJoinUsersRelatedByOid($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByOid relation
 *
 * @method     ChildLijstjespermsQuery joinWithUsersRelatedByOid($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByOid relation
 *
 * @method     ChildLijstjespermsQuery leftJoinWithUsersRelatedByOid() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByOid relation
 * @method     ChildLijstjespermsQuery rightJoinWithUsersRelatedByOid() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByOid relation
 * @method     ChildLijstjespermsQuery innerJoinWithUsersRelatedByOid() Adds a INNER JOIN clause and with to the query using the UsersRelatedByOid relation
 *
 * @method     ChildLijstjespermsQuery leftJoinUsersRelatedBySid($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedBySid relation
 * @method     ChildLijstjespermsQuery rightJoinUsersRelatedBySid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedBySid relation
 * @method     ChildLijstjespermsQuery innerJoinUsersRelatedBySid($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedBySid relation
 *
 * @method     ChildLijstjespermsQuery joinWithUsersRelatedBySid($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedBySid relation
 *
 * @method     ChildLijstjespermsQuery leftJoinWithUsersRelatedBySid() Adds a LEFT JOIN clause and with to the query using the UsersRelatedBySid relation
 * @method     ChildLijstjespermsQuery rightJoinWithUsersRelatedBySid() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedBySid relation
 * @method     ChildLijstjespermsQuery innerJoinWithUsersRelatedBySid() Adds a INNER JOIN clause and with to the query using the UsersRelatedBySid relation
 *
 * @method     ChildLijstjespermsQuery leftJoinLijstjes($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lijstjes relation
 * @method     ChildLijstjespermsQuery rightJoinLijstjes($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lijstjes relation
 * @method     ChildLijstjespermsQuery innerJoinLijstjes($relationAlias = null) Adds a INNER JOIN clause to the query using the Lijstjes relation
 *
 * @method     ChildLijstjespermsQuery joinWithLijstjes($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lijstjes relation
 *
 * @method     ChildLijstjespermsQuery leftJoinWithLijstjes() Adds a LEFT JOIN clause and with to the query using the Lijstjes relation
 * @method     ChildLijstjespermsQuery rightJoinWithLijstjes() Adds a RIGHT JOIN clause and with to the query using the Lijstjes relation
 * @method     ChildLijstjespermsQuery innerJoinWithLijstjes() Adds a INNER JOIN clause and with to the query using the Lijstjes relation
 *
 * @method     \UsersQuery|\LijstjesQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLijstjesperms findOne(ConnectionInterface $con = null) Return the first ChildLijstjesperms matching the query
 * @method     ChildLijstjesperms findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLijstjesperms matching the query, or a new ChildLijstjesperms object populated from the query conditions when no match is found
 *
 * @method     ChildLijstjesperms findOneById(int $ID) Return the first ChildLijstjesperms filtered by the ID column
 * @method     ChildLijstjesperms findOneByR(int $R) Return the first ChildLijstjesperms filtered by the R column
 * @method     ChildLijstjesperms findOneByA(int $A) Return the first ChildLijstjesperms filtered by the A column
 * @method     ChildLijstjesperms findOneByE(int $E) Return the first ChildLijstjesperms filtered by the E column
 * @method     ChildLijstjesperms findOneByS(int $S) Return the first ChildLijstjesperms filtered by the S column
 * @method     ChildLijstjesperms findOneByD(int $D) Return the first ChildLijstjesperms filtered by the D column
 * @method     ChildLijstjesperms findOneByOid(int $OID) Return the first ChildLijstjesperms filtered by the OID column
 * @method     ChildLijstjesperms findOneBySid(int $SID) Return the first ChildLijstjesperms filtered by the SID column
 * @method     ChildLijstjesperms findOneByLid(int $LID) Return the first ChildLijstjesperms filtered by the LID column *

 * @method     ChildLijstjesperms requirePk($key, ConnectionInterface $con = null) Return the ChildLijstjesperms by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOne(ConnectionInterface $con = null) Return the first ChildLijstjesperms matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLijstjesperms requireOneById(int $ID) Return the first ChildLijstjesperms filtered by the ID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneByR(int $R) Return the first ChildLijstjesperms filtered by the R column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneByA(int $A) Return the first ChildLijstjesperms filtered by the A column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneByE(int $E) Return the first ChildLijstjesperms filtered by the E column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneByS(int $S) Return the first ChildLijstjesperms filtered by the S column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneByD(int $D) Return the first ChildLijstjesperms filtered by the D column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneByOid(int $OID) Return the first ChildLijstjesperms filtered by the OID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneBySid(int $SID) Return the first ChildLijstjesperms filtered by the SID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjesperms requireOneByLid(int $LID) Return the first ChildLijstjesperms filtered by the LID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLijstjesperms[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLijstjesperms objects based on current ModelCriteria
 * @method     ChildLijstjesperms[]|ObjectCollection findById(int $ID) Return ChildLijstjesperms objects filtered by the ID column
 * @method     ChildLijstjesperms[]|ObjectCollection findByR(int $R) Return ChildLijstjesperms objects filtered by the R column
 * @method     ChildLijstjesperms[]|ObjectCollection findByA(int $A) Return ChildLijstjesperms objects filtered by the A column
 * @method     ChildLijstjesperms[]|ObjectCollection findByE(int $E) Return ChildLijstjesperms objects filtered by the E column
 * @method     ChildLijstjesperms[]|ObjectCollection findByS(int $S) Return ChildLijstjesperms objects filtered by the S column
 * @method     ChildLijstjesperms[]|ObjectCollection findByD(int $D) Return ChildLijstjesperms objects filtered by the D column
 * @method     ChildLijstjesperms[]|ObjectCollection findByOid(int $OID) Return ChildLijstjesperms objects filtered by the OID column
 * @method     ChildLijstjesperms[]|ObjectCollection findBySid(int $SID) Return ChildLijstjesperms objects filtered by the SID column
 * @method     ChildLijstjesperms[]|ObjectCollection findByLid(int $LID) Return ChildLijstjesperms objects filtered by the LID column
 * @method     ChildLijstjesperms[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LijstjespermsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\LijstjespermsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Lijstjesperms', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLijstjespermsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLijstjespermsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLijstjespermsQuery) {
            return $criteria;
        }
        $query = new ChildLijstjespermsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLijstjesperms|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LijstjespermsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LijstjespermsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLijstjesperms A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT ID, R, A, E, S, D, OID, SID, LID FROM LijstjesPerms WHERE ID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLijstjesperms $obj */
            $obj = new ChildLijstjesperms();
            $obj->hydrate($row);
            LijstjespermsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLijstjesperms|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LijstjespermsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LijstjespermsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the R column
     *
     * Example usage:
     * <code>
     * $query->filterByR(1234); // WHERE R = 1234
     * $query->filterByR(array(12, 34)); // WHERE R IN (12, 34)
     * $query->filterByR(array('min' => 12)); // WHERE R > 12
     * </code>
     *
     * @param     mixed $r The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByR($r = null, $comparison = null)
    {
        if (is_array($r)) {
            $useMinMax = false;
            if (isset($r['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_R, $r['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($r['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_R, $r['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_R, $r, $comparison);
    }

    /**
     * Filter the query on the A column
     *
     * Example usage:
     * <code>
     * $query->filterByA(1234); // WHERE A = 1234
     * $query->filterByA(array(12, 34)); // WHERE A IN (12, 34)
     * $query->filterByA(array('min' => 12)); // WHERE A > 12
     * </code>
     *
     * @param     mixed $a The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByA($a = null, $comparison = null)
    {
        if (is_array($a)) {
            $useMinMax = false;
            if (isset($a['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_A, $a['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($a['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_A, $a['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_A, $a, $comparison);
    }

    /**
     * Filter the query on the E column
     *
     * Example usage:
     * <code>
     * $query->filterByE(1234); // WHERE E = 1234
     * $query->filterByE(array(12, 34)); // WHERE E IN (12, 34)
     * $query->filterByE(array('min' => 12)); // WHERE E > 12
     * </code>
     *
     * @param     mixed $e The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByE($e = null, $comparison = null)
    {
        if (is_array($e)) {
            $useMinMax = false;
            if (isset($e['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_E, $e['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($e['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_E, $e['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_E, $e, $comparison);
    }

    /**
     * Filter the query on the S column
     *
     * Example usage:
     * <code>
     * $query->filterByS(1234); // WHERE S = 1234
     * $query->filterByS(array(12, 34)); // WHERE S IN (12, 34)
     * $query->filterByS(array('min' => 12)); // WHERE S > 12
     * </code>
     *
     * @param     mixed $s The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByS($s = null, $comparison = null)
    {
        if (is_array($s)) {
            $useMinMax = false;
            if (isset($s['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_S, $s['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($s['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_S, $s['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_S, $s, $comparison);
    }

    /**
     * Filter the query on the D column
     *
     * Example usage:
     * <code>
     * $query->filterByD(1234); // WHERE D = 1234
     * $query->filterByD(array(12, 34)); // WHERE D IN (12, 34)
     * $query->filterByD(array('min' => 12)); // WHERE D > 12
     * </code>
     *
     * @param     mixed $d The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByD($d = null, $comparison = null)
    {
        if (is_array($d)) {
            $useMinMax = false;
            if (isset($d['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_D, $d['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($d['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_D, $d['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_D, $d, $comparison);
    }

    /**
     * Filter the query on the OID column
     *
     * Example usage:
     * <code>
     * $query->filterByOid(1234); // WHERE OID = 1234
     * $query->filterByOid(array(12, 34)); // WHERE OID IN (12, 34)
     * $query->filterByOid(array('min' => 12)); // WHERE OID > 12
     * </code>
     *
     * @see       filterByUsersRelatedByOid()
     *
     * @param     mixed $oid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByOid($oid = null, $comparison = null)
    {
        if (is_array($oid)) {
            $useMinMax = false;
            if (isset($oid['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_OID, $oid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oid['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_OID, $oid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_OID, $oid, $comparison);
    }

    /**
     * Filter the query on the SID column
     *
     * Example usage:
     * <code>
     * $query->filterBySid(1234); // WHERE SID = 1234
     * $query->filterBySid(array(12, 34)); // WHERE SID IN (12, 34)
     * $query->filterBySid(array('min' => 12)); // WHERE SID > 12
     * </code>
     *
     * @see       filterByUsersRelatedBySid()
     *
     * @param     mixed $sid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterBySid($sid = null, $comparison = null)
    {
        if (is_array($sid)) {
            $useMinMax = false;
            if (isset($sid['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_SID, $sid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sid['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_SID, $sid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_SID, $sid, $comparison);
    }

    /**
     * Filter the query on the LID column
     *
     * Example usage:
     * <code>
     * $query->filterByLid(1234); // WHERE LID = 1234
     * $query->filterByLid(array(12, 34)); // WHERE LID IN (12, 34)
     * $query->filterByLid(array('min' => 12)); // WHERE LID > 12
     * </code>
     *
     * @see       filterByLijstjes()
     *
     * @param     mixed $lid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByLid($lid = null, $comparison = null)
    {
        if (is_array($lid)) {
            $useMinMax = false;
            if (isset($lid['min'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_LID, $lid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lid['max'])) {
                $this->addUsingAlias(LijstjespermsTableMap::COL_LID, $lid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjespermsTableMap::COL_LID, $lid, $comparison);
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByOid($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(LijstjespermsTableMap::COL_OID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LijstjespermsTableMap::COL_OID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByOid() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByOid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByOid($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByOid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByOid');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByOid relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByOidQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsersRelatedByOid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByOid', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedBySid($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(LijstjespermsTableMap::COL_SID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LijstjespermsTableMap::COL_SID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedBySid() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedBySid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function joinUsersRelatedBySid($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedBySid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedBySid');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedBySid relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedBySidQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsersRelatedBySid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedBySid', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Lijstjes object
     *
     * @param \Lijstjes|ObjectCollection $lijstjes The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function filterByLijstjes($lijstjes, $comparison = null)
    {
        if ($lijstjes instanceof \Lijstjes) {
            return $this
                ->addUsingAlias(LijstjespermsTableMap::COL_LID, $lijstjes->getLid(), $comparison);
        } elseif ($lijstjes instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LijstjespermsTableMap::COL_LID, $lijstjes->toKeyValue('PrimaryKey', 'Lid'), $comparison);
        } else {
            throw new PropelException('filterByLijstjes() only accepts arguments of type \Lijstjes or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lijstjes relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function joinLijstjes($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lijstjes');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lijstjes');
        }

        return $this;
    }

    /**
     * Use the Lijstjes relation Lijstjes object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LijstjesQuery A secondary query class using the current class as primary query
     */
    public function useLijstjesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLijstjes($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lijstjes', '\LijstjesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLijstjesperms $lijstjesperms Object to remove from the list of results
     *
     * @return $this|ChildLijstjespermsQuery The current query, for fluid interface
     */
    public function prune($lijstjesperms = null)
    {
        if ($lijstjesperms) {
            $this->addUsingAlias(LijstjespermsTableMap::COL_ID, $lijstjesperms->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the LijstjesPerms table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LijstjespermsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LijstjespermsTableMap::clearInstancePool();
            LijstjespermsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LijstjespermsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LijstjespermsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LijstjespermsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LijstjespermsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LijstjespermsQuery
