<?php

namespace Base;

use \Activationtokens as ChildActivationtokens;
use \ActivationtokensQuery as ChildActivationtokensQuery;
use \Authtokens as ChildAuthtokens;
use \AuthtokensQuery as ChildAuthtokensQuery;
use \Lijstjes as ChildLijstjes;
use \LijstjesQuery as ChildLijstjesQuery;
use \Lijstjesperms as ChildLijstjesperms;
use \LijstjespermsQuery as ChildLijstjespermsQuery;
use \Users as ChildUsers;
use \UsersQuery as ChildUsersQuery;
use \Exception;
use \PDO;
use Map\ActivationtokensTableMap;
use Map\AuthtokensTableMap;
use Map\LijstjesTableMap;
use Map\LijstjespermsTableMap;
use Map\UsersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'Users' table.
 *
 *
 *
 * @package    propel.generator.default.Base
 */
abstract class Users implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\UsersTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the voornaam field.
     *
     * @var        string
     */
    protected $voornaam;

    /**
     * The value for the achternaam field.
     *
     * @var        string
     */
    protected $achternaam;

    /**
     * The value for the username field.
     *
     * @var        string
     */
    protected $username;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the isactivated field.
     *
     * @var        int
     */
    protected $isactivated;

    /**
     * @var        ObjectCollection|ChildAuthtokens[] Collection to store aggregation of ChildAuthtokens objects.
     */
    protected $collAuthtokenss;
    protected $collAuthtokenssPartial;

    /**
     * @var        ObjectCollection|ChildLijstjes[] Collection to store aggregation of ChildLijstjes objects.
     */
    protected $collLijstjess;
    protected $collLijstjessPartial;

    /**
     * @var        ObjectCollection|ChildLijstjesperms[] Collection to store aggregation of ChildLijstjesperms objects.
     */
    protected $collLijstjespermssRelatedByOid;
    protected $collLijstjespermssRelatedByOidPartial;

    /**
     * @var        ObjectCollection|ChildLijstjesperms[] Collection to store aggregation of ChildLijstjesperms objects.
     */
    protected $collLijstjespermssRelatedBySid;
    protected $collLijstjespermssRelatedBySidPartial;

    /**
     * @var        ObjectCollection|ChildActivationtokens[] Collection to store aggregation of ChildActivationtokens objects.
     */
    protected $collActivationtokenss;
    protected $collActivationtokenssPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAuthtokens[]
     */
    protected $authtokenssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLijstjes[]
     */
    protected $lijstjessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLijstjesperms[]
     */
    protected $lijstjespermssRelatedByOidScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLijstjesperms[]
     */
    protected $lijstjespermssRelatedBySidScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildActivationtokens[]
     */
    protected $activationtokenssScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Users object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Users</code> instance.  If
     * <code>obj</code> is an instance of <code>Users</code>, delegates to
     * <code>equals(Users)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Users The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [voornaam] column value.
     *
     * @return string
     */
    public function getVoornaam()
    {
        return $this->voornaam;
    }

    /**
     * Get the [achternaam] column value.
     *
     * @return string
     */
    public function getAchternaam()
    {
        return $this->achternaam;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [isactivated] column value.
     *
     * @return int
     */
    public function getIsactivated()
    {
        return $this->isactivated;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Users The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UsersTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [voornaam] column.
     *
     * @param string $v new value
     * @return $this|\Users The current object (for fluent API support)
     */
    public function setVoornaam($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->voornaam !== $v) {
            $this->voornaam = $v;
            $this->modifiedColumns[UsersTableMap::COL_VOORNAAM] = true;
        }

        return $this;
    } // setVoornaam()

    /**
     * Set the value of [achternaam] column.
     *
     * @param string $v new value
     * @return $this|\Users The current object (for fluent API support)
     */
    public function setAchternaam($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->achternaam !== $v) {
            $this->achternaam = $v;
            $this->modifiedColumns[UsersTableMap::COL_ACHTERNAAM] = true;
        }

        return $this;
    } // setAchternaam()

    /**
     * Set the value of [username] column.
     *
     * @param string $v new value
     * @return $this|\Users The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[UsersTableMap::COL_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\Users The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[UsersTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return $this|\Users The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[UsersTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [isactivated] column.
     *
     * @param int $v new value
     * @return $this|\Users The current object (for fluent API support)
     */
    public function setIsactivated($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->isactivated !== $v) {
            $this->isactivated = $v;
            $this->modifiedColumns[UsersTableMap::COL_ISACTIVATED] = true;
        }

        return $this;
    } // setIsactivated()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UsersTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UsersTableMap::translateFieldName('Voornaam', TableMap::TYPE_PHPNAME, $indexType)];
            $this->voornaam = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UsersTableMap::translateFieldName('Achternaam', TableMap::TYPE_PHPNAME, $indexType)];
            $this->achternaam = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UsersTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UsersTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UsersTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UsersTableMap::translateFieldName('Isactivated', TableMap::TYPE_PHPNAME, $indexType)];
            $this->isactivated = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = UsersTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Users'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsersTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUsersQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collAuthtokenss = null;

            $this->collLijstjess = null;

            $this->collLijstjespermssRelatedByOid = null;

            $this->collLijstjespermssRelatedBySid = null;

            $this->collActivationtokenss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Users::setDeleted()
     * @see Users::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUsersQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UsersTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->authtokenssScheduledForDeletion !== null) {
                if (!$this->authtokenssScheduledForDeletion->isEmpty()) {
                    \AuthtokensQuery::create()
                        ->filterByPrimaryKeys($this->authtokenssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->authtokenssScheduledForDeletion = null;
                }
            }

            if ($this->collAuthtokenss !== null) {
                foreach ($this->collAuthtokenss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lijstjessScheduledForDeletion !== null) {
                if (!$this->lijstjessScheduledForDeletion->isEmpty()) {
                    \LijstjesQuery::create()
                        ->filterByPrimaryKeys($this->lijstjessScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lijstjessScheduledForDeletion = null;
                }
            }

            if ($this->collLijstjess !== null) {
                foreach ($this->collLijstjess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lijstjespermssRelatedByOidScheduledForDeletion !== null) {
                if (!$this->lijstjespermssRelatedByOidScheduledForDeletion->isEmpty()) {
                    \LijstjespermsQuery::create()
                        ->filterByPrimaryKeys($this->lijstjespermssRelatedByOidScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lijstjespermssRelatedByOidScheduledForDeletion = null;
                }
            }

            if ($this->collLijstjespermssRelatedByOid !== null) {
                foreach ($this->collLijstjespermssRelatedByOid as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lijstjespermssRelatedBySidScheduledForDeletion !== null) {
                if (!$this->lijstjespermssRelatedBySidScheduledForDeletion->isEmpty()) {
                    \LijstjespermsQuery::create()
                        ->filterByPrimaryKeys($this->lijstjespermssRelatedBySidScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lijstjespermssRelatedBySidScheduledForDeletion = null;
                }
            }

            if ($this->collLijstjespermssRelatedBySid !== null) {
                foreach ($this->collLijstjespermssRelatedBySid as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->activationtokenssScheduledForDeletion !== null) {
                if (!$this->activationtokenssScheduledForDeletion->isEmpty()) {
                    \ActivationtokensQuery::create()
                        ->filterByPrimaryKeys($this->activationtokenssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->activationtokenssScheduledForDeletion = null;
                }
            }

            if ($this->collActivationtokenss !== null) {
                foreach ($this->collActivationtokenss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UsersTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UsersTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UsersTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ID';
        }
        if ($this->isColumnModified(UsersTableMap::COL_VOORNAAM)) {
            $modifiedColumns[':p' . $index++]  = 'Voornaam';
        }
        if ($this->isColumnModified(UsersTableMap::COL_ACHTERNAAM)) {
            $modifiedColumns[':p' . $index++]  = 'Achternaam';
        }
        if ($this->isColumnModified(UsersTableMap::COL_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'Username';
        }
        if ($this->isColumnModified(UsersTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(UsersTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'Password';
        }
        if ($this->isColumnModified(UsersTableMap::COL_ISACTIVATED)) {
            $modifiedColumns[':p' . $index++]  = 'isActivated';
        }

        $sql = sprintf(
            'INSERT INTO Users (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'ID':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'Voornaam':
                        $stmt->bindValue($identifier, $this->voornaam, PDO::PARAM_STR);
                        break;
                    case 'Achternaam':
                        $stmt->bindValue($identifier, $this->achternaam, PDO::PARAM_STR);
                        break;
                    case 'Username':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'Password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'isActivated':
                        $stmt->bindValue($identifier, $this->isactivated, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getVoornaam();
                break;
            case 2:
                return $this->getAchternaam();
                break;
            case 3:
                return $this->getUsername();
                break;
            case 4:
                return $this->getEmail();
                break;
            case 5:
                return $this->getPassword();
                break;
            case 6:
                return $this->getIsactivated();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Users'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Users'][$this->hashCode()] = true;
        $keys = UsersTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getVoornaam(),
            $keys[2] => $this->getAchternaam(),
            $keys[3] => $this->getUsername(),
            $keys[4] => $this->getEmail(),
            $keys[5] => $this->getPassword(),
            $keys[6] => $this->getIsactivated(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collAuthtokenss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'authtokenss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Authtokenss';
                        break;
                    default:
                        $key = 'Authtokenss';
                }

                $result[$key] = $this->collAuthtokenss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLijstjess) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lijstjess';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Lijstjess';
                        break;
                    default:
                        $key = 'Lijstjess';
                }

                $result[$key] = $this->collLijstjess->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLijstjespermssRelatedByOid) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lijstjespermss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'LijstjesPermss';
                        break;
                    default:
                        $key = 'Lijstjespermss';
                }

                $result[$key] = $this->collLijstjespermssRelatedByOid->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLijstjespermssRelatedBySid) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lijstjespermss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'LijstjesPermss';
                        break;
                    default:
                        $key = 'Lijstjespermss';
                }

                $result[$key] = $this->collLijstjespermssRelatedBySid->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collActivationtokenss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'activationtokenss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'activationTokenss';
                        break;
                    default:
                        $key = 'Activationtokenss';
                }

                $result[$key] = $this->collActivationtokenss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Users
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Users
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setVoornaam($value);
                break;
            case 2:
                $this->setAchternaam($value);
                break;
            case 3:
                $this->setUsername($value);
                break;
            case 4:
                $this->setEmail($value);
                break;
            case 5:
                $this->setPassword($value);
                break;
            case 6:
                $this->setIsactivated($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UsersTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setVoornaam($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAchternaam($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setUsername($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setEmail($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPassword($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIsactivated($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Users The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UsersTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UsersTableMap::COL_ID)) {
            $criteria->add(UsersTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UsersTableMap::COL_VOORNAAM)) {
            $criteria->add(UsersTableMap::COL_VOORNAAM, $this->voornaam);
        }
        if ($this->isColumnModified(UsersTableMap::COL_ACHTERNAAM)) {
            $criteria->add(UsersTableMap::COL_ACHTERNAAM, $this->achternaam);
        }
        if ($this->isColumnModified(UsersTableMap::COL_USERNAME)) {
            $criteria->add(UsersTableMap::COL_USERNAME, $this->username);
        }
        if ($this->isColumnModified(UsersTableMap::COL_EMAIL)) {
            $criteria->add(UsersTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(UsersTableMap::COL_PASSWORD)) {
            $criteria->add(UsersTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(UsersTableMap::COL_ISACTIVATED)) {
            $criteria->add(UsersTableMap::COL_ISACTIVATED, $this->isactivated);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUsersQuery::create();
        $criteria->add(UsersTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Users (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setVoornaam($this->getVoornaam());
        $copyObj->setAchternaam($this->getAchternaam());
        $copyObj->setUsername($this->getUsername());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setIsactivated($this->getIsactivated());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getAuthtokenss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAuthtokens($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLijstjess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLijstjes($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLijstjespermssRelatedByOid() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLijstjespermsRelatedByOid($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLijstjespermssRelatedBySid() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLijstjespermsRelatedBySid($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getActivationtokenss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addActivationtokens($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Users Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Authtokens' == $relationName) {
            $this->initAuthtokenss();
            return;
        }
        if ('Lijstjes' == $relationName) {
            $this->initLijstjess();
            return;
        }
        if ('LijstjespermsRelatedByOid' == $relationName) {
            $this->initLijstjespermssRelatedByOid();
            return;
        }
        if ('LijstjespermsRelatedBySid' == $relationName) {
            $this->initLijstjespermssRelatedBySid();
            return;
        }
        if ('Activationtokens' == $relationName) {
            $this->initActivationtokenss();
            return;
        }
    }

    /**
     * Clears out the collAuthtokenss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addAuthtokenss()
     */
    public function clearAuthtokenss()
    {
        $this->collAuthtokenss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collAuthtokenss collection loaded partially.
     */
    public function resetPartialAuthtokenss($v = true)
    {
        $this->collAuthtokenssPartial = $v;
    }

    /**
     * Initializes the collAuthtokenss collection.
     *
     * By default this just sets the collAuthtokenss collection to an empty array (like clearcollAuthtokenss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAuthtokenss($overrideExisting = true)
    {
        if (null !== $this->collAuthtokenss && !$overrideExisting) {
            return;
        }

        $collectionClassName = AuthtokensTableMap::getTableMap()->getCollectionClassName();

        $this->collAuthtokenss = new $collectionClassName;
        $this->collAuthtokenss->setModel('\Authtokens');
    }

    /**
     * Gets an array of ChildAuthtokens objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAuthtokens[] List of ChildAuthtokens objects
     * @throws PropelException
     */
    public function getAuthtokenss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collAuthtokenssPartial && !$this->isNew();
        if (null === $this->collAuthtokenss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAuthtokenss) {
                // return empty collection
                $this->initAuthtokenss();
            } else {
                $collAuthtokenss = ChildAuthtokensQuery::create(null, $criteria)
                    ->filterByUsers($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAuthtokenssPartial && count($collAuthtokenss)) {
                        $this->initAuthtokenss(false);

                        foreach ($collAuthtokenss as $obj) {
                            if (false == $this->collAuthtokenss->contains($obj)) {
                                $this->collAuthtokenss->append($obj);
                            }
                        }

                        $this->collAuthtokenssPartial = true;
                    }

                    return $collAuthtokenss;
                }

                if ($partial && $this->collAuthtokenss) {
                    foreach ($this->collAuthtokenss as $obj) {
                        if ($obj->isNew()) {
                            $collAuthtokenss[] = $obj;
                        }
                    }
                }

                $this->collAuthtokenss = $collAuthtokenss;
                $this->collAuthtokenssPartial = false;
            }
        }

        return $this->collAuthtokenss;
    }

    /**
     * Sets a collection of ChildAuthtokens objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $authtokenss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setAuthtokenss(Collection $authtokenss, ConnectionInterface $con = null)
    {
        /** @var ChildAuthtokens[] $authtokenssToDelete */
        $authtokenssToDelete = $this->getAuthtokenss(new Criteria(), $con)->diff($authtokenss);


        $this->authtokenssScheduledForDeletion = $authtokenssToDelete;

        foreach ($authtokenssToDelete as $authtokensRemoved) {
            $authtokensRemoved->setUsers(null);
        }

        $this->collAuthtokenss = null;
        foreach ($authtokenss as $authtokens) {
            $this->addAuthtokens($authtokens);
        }

        $this->collAuthtokenss = $authtokenss;
        $this->collAuthtokenssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Authtokens objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Authtokens objects.
     * @throws PropelException
     */
    public function countAuthtokenss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collAuthtokenssPartial && !$this->isNew();
        if (null === $this->collAuthtokenss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAuthtokenss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAuthtokenss());
            }

            $query = ChildAuthtokensQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsers($this)
                ->count($con);
        }

        return count($this->collAuthtokenss);
    }

    /**
     * Method called to associate a ChildAuthtokens object to this object
     * through the ChildAuthtokens foreign key attribute.
     *
     * @param  ChildAuthtokens $l ChildAuthtokens
     * @return $this|\Users The current object (for fluent API support)
     */
    public function addAuthtokens(ChildAuthtokens $l)
    {
        if ($this->collAuthtokenss === null) {
            $this->initAuthtokenss();
            $this->collAuthtokenssPartial = true;
        }

        if (!$this->collAuthtokenss->contains($l)) {
            $this->doAddAuthtokens($l);

            if ($this->authtokenssScheduledForDeletion and $this->authtokenssScheduledForDeletion->contains($l)) {
                $this->authtokenssScheduledForDeletion->remove($this->authtokenssScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAuthtokens $authtokens The ChildAuthtokens object to add.
     */
    protected function doAddAuthtokens(ChildAuthtokens $authtokens)
    {
        $this->collAuthtokenss[]= $authtokens;
        $authtokens->setUsers($this);
    }

    /**
     * @param  ChildAuthtokens $authtokens The ChildAuthtokens object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeAuthtokens(ChildAuthtokens $authtokens)
    {
        if ($this->getAuthtokenss()->contains($authtokens)) {
            $pos = $this->collAuthtokenss->search($authtokens);
            $this->collAuthtokenss->remove($pos);
            if (null === $this->authtokenssScheduledForDeletion) {
                $this->authtokenssScheduledForDeletion = clone $this->collAuthtokenss;
                $this->authtokenssScheduledForDeletion->clear();
            }
            $this->authtokenssScheduledForDeletion[]= $authtokens;
            $authtokens->setUsers(null);
        }

        return $this;
    }

    /**
     * Clears out the collLijstjess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLijstjess()
     */
    public function clearLijstjess()
    {
        $this->collLijstjess = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLijstjess collection loaded partially.
     */
    public function resetPartialLijstjess($v = true)
    {
        $this->collLijstjessPartial = $v;
    }

    /**
     * Initializes the collLijstjess collection.
     *
     * By default this just sets the collLijstjess collection to an empty array (like clearcollLijstjess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLijstjess($overrideExisting = true)
    {
        if (null !== $this->collLijstjess && !$overrideExisting) {
            return;
        }

        $collectionClassName = LijstjesTableMap::getTableMap()->getCollectionClassName();

        $this->collLijstjess = new $collectionClassName;
        $this->collLijstjess->setModel('\Lijstjes');
    }

    /**
     * Gets an array of ChildLijstjes objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLijstjes[] List of ChildLijstjes objects
     * @throws PropelException
     */
    public function getLijstjess(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLijstjessPartial && !$this->isNew();
        if (null === $this->collLijstjess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLijstjess) {
                // return empty collection
                $this->initLijstjess();
            } else {
                $collLijstjess = ChildLijstjesQuery::create(null, $criteria)
                    ->filterByUsers($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLijstjessPartial && count($collLijstjess)) {
                        $this->initLijstjess(false);

                        foreach ($collLijstjess as $obj) {
                            if (false == $this->collLijstjess->contains($obj)) {
                                $this->collLijstjess->append($obj);
                            }
                        }

                        $this->collLijstjessPartial = true;
                    }

                    return $collLijstjess;
                }

                if ($partial && $this->collLijstjess) {
                    foreach ($this->collLijstjess as $obj) {
                        if ($obj->isNew()) {
                            $collLijstjess[] = $obj;
                        }
                    }
                }

                $this->collLijstjess = $collLijstjess;
                $this->collLijstjessPartial = false;
            }
        }

        return $this->collLijstjess;
    }

    /**
     * Sets a collection of ChildLijstjes objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lijstjess A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setLijstjess(Collection $lijstjess, ConnectionInterface $con = null)
    {
        /** @var ChildLijstjes[] $lijstjessToDelete */
        $lijstjessToDelete = $this->getLijstjess(new Criteria(), $con)->diff($lijstjess);


        $this->lijstjessScheduledForDeletion = $lijstjessToDelete;

        foreach ($lijstjessToDelete as $lijstjesRemoved) {
            $lijstjesRemoved->setUsers(null);
        }

        $this->collLijstjess = null;
        foreach ($lijstjess as $lijstjes) {
            $this->addLijstjes($lijstjes);
        }

        $this->collLijstjess = $lijstjess;
        $this->collLijstjessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Lijstjes objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Lijstjes objects.
     * @throws PropelException
     */
    public function countLijstjess(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLijstjessPartial && !$this->isNew();
        if (null === $this->collLijstjess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLijstjess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLijstjess());
            }

            $query = ChildLijstjesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsers($this)
                ->count($con);
        }

        return count($this->collLijstjess);
    }

    /**
     * Method called to associate a ChildLijstjes object to this object
     * through the ChildLijstjes foreign key attribute.
     *
     * @param  ChildLijstjes $l ChildLijstjes
     * @return $this|\Users The current object (for fluent API support)
     */
    public function addLijstjes(ChildLijstjes $l)
    {
        if ($this->collLijstjess === null) {
            $this->initLijstjess();
            $this->collLijstjessPartial = true;
        }

        if (!$this->collLijstjess->contains($l)) {
            $this->doAddLijstjes($l);

            if ($this->lijstjessScheduledForDeletion and $this->lijstjessScheduledForDeletion->contains($l)) {
                $this->lijstjessScheduledForDeletion->remove($this->lijstjessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLijstjes $lijstjes The ChildLijstjes object to add.
     */
    protected function doAddLijstjes(ChildLijstjes $lijstjes)
    {
        $this->collLijstjess[]= $lijstjes;
        $lijstjes->setUsers($this);
    }

    /**
     * @param  ChildLijstjes $lijstjes The ChildLijstjes object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeLijstjes(ChildLijstjes $lijstjes)
    {
        if ($this->getLijstjess()->contains($lijstjes)) {
            $pos = $this->collLijstjess->search($lijstjes);
            $this->collLijstjess->remove($pos);
            if (null === $this->lijstjessScheduledForDeletion) {
                $this->lijstjessScheduledForDeletion = clone $this->collLijstjess;
                $this->lijstjessScheduledForDeletion->clear();
            }
            $this->lijstjessScheduledForDeletion[]= $lijstjes;
            $lijstjes->setUsers(null);
        }

        return $this;
    }

    /**
     * Clears out the collLijstjespermssRelatedByOid collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLijstjespermssRelatedByOid()
     */
    public function clearLijstjespermssRelatedByOid()
    {
        $this->collLijstjespermssRelatedByOid = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLijstjespermssRelatedByOid collection loaded partially.
     */
    public function resetPartialLijstjespermssRelatedByOid($v = true)
    {
        $this->collLijstjespermssRelatedByOidPartial = $v;
    }

    /**
     * Initializes the collLijstjespermssRelatedByOid collection.
     *
     * By default this just sets the collLijstjespermssRelatedByOid collection to an empty array (like clearcollLijstjespermssRelatedByOid());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLijstjespermssRelatedByOid($overrideExisting = true)
    {
        if (null !== $this->collLijstjespermssRelatedByOid && !$overrideExisting) {
            return;
        }

        $collectionClassName = LijstjespermsTableMap::getTableMap()->getCollectionClassName();

        $this->collLijstjespermssRelatedByOid = new $collectionClassName;
        $this->collLijstjespermssRelatedByOid->setModel('\Lijstjesperms');
    }

    /**
     * Gets an array of ChildLijstjesperms objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLijstjesperms[] List of ChildLijstjesperms objects
     * @throws PropelException
     */
    public function getLijstjespermssRelatedByOid(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLijstjespermssRelatedByOidPartial && !$this->isNew();
        if (null === $this->collLijstjespermssRelatedByOid || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLijstjespermssRelatedByOid) {
                // return empty collection
                $this->initLijstjespermssRelatedByOid();
            } else {
                $collLijstjespermssRelatedByOid = ChildLijstjespermsQuery::create(null, $criteria)
                    ->filterByUsersRelatedByOid($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLijstjespermssRelatedByOidPartial && count($collLijstjespermssRelatedByOid)) {
                        $this->initLijstjespermssRelatedByOid(false);

                        foreach ($collLijstjespermssRelatedByOid as $obj) {
                            if (false == $this->collLijstjespermssRelatedByOid->contains($obj)) {
                                $this->collLijstjespermssRelatedByOid->append($obj);
                            }
                        }

                        $this->collLijstjespermssRelatedByOidPartial = true;
                    }

                    return $collLijstjespermssRelatedByOid;
                }

                if ($partial && $this->collLijstjespermssRelatedByOid) {
                    foreach ($this->collLijstjespermssRelatedByOid as $obj) {
                        if ($obj->isNew()) {
                            $collLijstjespermssRelatedByOid[] = $obj;
                        }
                    }
                }

                $this->collLijstjespermssRelatedByOid = $collLijstjespermssRelatedByOid;
                $this->collLijstjespermssRelatedByOidPartial = false;
            }
        }

        return $this->collLijstjespermssRelatedByOid;
    }

    /**
     * Sets a collection of ChildLijstjesperms objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lijstjespermssRelatedByOid A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setLijstjespermssRelatedByOid(Collection $lijstjespermssRelatedByOid, ConnectionInterface $con = null)
    {
        /** @var ChildLijstjesperms[] $lijstjespermssRelatedByOidToDelete */
        $lijstjespermssRelatedByOidToDelete = $this->getLijstjespermssRelatedByOid(new Criteria(), $con)->diff($lijstjespermssRelatedByOid);


        $this->lijstjespermssRelatedByOidScheduledForDeletion = $lijstjespermssRelatedByOidToDelete;

        foreach ($lijstjespermssRelatedByOidToDelete as $lijstjespermsRelatedByOidRemoved) {
            $lijstjespermsRelatedByOidRemoved->setUsersRelatedByOid(null);
        }

        $this->collLijstjespermssRelatedByOid = null;
        foreach ($lijstjespermssRelatedByOid as $lijstjespermsRelatedByOid) {
            $this->addLijstjespermsRelatedByOid($lijstjespermsRelatedByOid);
        }

        $this->collLijstjespermssRelatedByOid = $lijstjespermssRelatedByOid;
        $this->collLijstjespermssRelatedByOidPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Lijstjesperms objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Lijstjesperms objects.
     * @throws PropelException
     */
    public function countLijstjespermssRelatedByOid(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLijstjespermssRelatedByOidPartial && !$this->isNew();
        if (null === $this->collLijstjespermssRelatedByOid || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLijstjespermssRelatedByOid) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLijstjespermssRelatedByOid());
            }

            $query = ChildLijstjespermsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsersRelatedByOid($this)
                ->count($con);
        }

        return count($this->collLijstjespermssRelatedByOid);
    }

    /**
     * Method called to associate a ChildLijstjesperms object to this object
     * through the ChildLijstjesperms foreign key attribute.
     *
     * @param  ChildLijstjesperms $l ChildLijstjesperms
     * @return $this|\Users The current object (for fluent API support)
     */
    public function addLijstjespermsRelatedByOid(ChildLijstjesperms $l)
    {
        if ($this->collLijstjespermssRelatedByOid === null) {
            $this->initLijstjespermssRelatedByOid();
            $this->collLijstjespermssRelatedByOidPartial = true;
        }

        if (!$this->collLijstjespermssRelatedByOid->contains($l)) {
            $this->doAddLijstjespermsRelatedByOid($l);

            if ($this->lijstjespermssRelatedByOidScheduledForDeletion and $this->lijstjespermssRelatedByOidScheduledForDeletion->contains($l)) {
                $this->lijstjespermssRelatedByOidScheduledForDeletion->remove($this->lijstjespermssRelatedByOidScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLijstjesperms $lijstjespermsRelatedByOid The ChildLijstjesperms object to add.
     */
    protected function doAddLijstjespermsRelatedByOid(ChildLijstjesperms $lijstjespermsRelatedByOid)
    {
        $this->collLijstjespermssRelatedByOid[]= $lijstjespermsRelatedByOid;
        $lijstjespermsRelatedByOid->setUsersRelatedByOid($this);
    }

    /**
     * @param  ChildLijstjesperms $lijstjespermsRelatedByOid The ChildLijstjesperms object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeLijstjespermsRelatedByOid(ChildLijstjesperms $lijstjespermsRelatedByOid)
    {
        if ($this->getLijstjespermssRelatedByOid()->contains($lijstjespermsRelatedByOid)) {
            $pos = $this->collLijstjespermssRelatedByOid->search($lijstjespermsRelatedByOid);
            $this->collLijstjespermssRelatedByOid->remove($pos);
            if (null === $this->lijstjespermssRelatedByOidScheduledForDeletion) {
                $this->lijstjespermssRelatedByOidScheduledForDeletion = clone $this->collLijstjespermssRelatedByOid;
                $this->lijstjespermssRelatedByOidScheduledForDeletion->clear();
            }
            $this->lijstjespermssRelatedByOidScheduledForDeletion[]= $lijstjespermsRelatedByOid;
            $lijstjespermsRelatedByOid->setUsersRelatedByOid(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Users is new, it will return
     * an empty collection; or if this Users has previously
     * been saved, it will retrieve related LijstjespermssRelatedByOid from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Users.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLijstjesperms[] List of ChildLijstjesperms objects
     */
    public function getLijstjespermssRelatedByOidJoinLijstjes(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLijstjespermsQuery::create(null, $criteria);
        $query->joinWith('Lijstjes', $joinBehavior);

        return $this->getLijstjespermssRelatedByOid($query, $con);
    }

    /**
     * Clears out the collLijstjespermssRelatedBySid collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLijstjespermssRelatedBySid()
     */
    public function clearLijstjespermssRelatedBySid()
    {
        $this->collLijstjespermssRelatedBySid = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLijstjespermssRelatedBySid collection loaded partially.
     */
    public function resetPartialLijstjespermssRelatedBySid($v = true)
    {
        $this->collLijstjespermssRelatedBySidPartial = $v;
    }

    /**
     * Initializes the collLijstjespermssRelatedBySid collection.
     *
     * By default this just sets the collLijstjespermssRelatedBySid collection to an empty array (like clearcollLijstjespermssRelatedBySid());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLijstjespermssRelatedBySid($overrideExisting = true)
    {
        if (null !== $this->collLijstjespermssRelatedBySid && !$overrideExisting) {
            return;
        }

        $collectionClassName = LijstjespermsTableMap::getTableMap()->getCollectionClassName();

        $this->collLijstjespermssRelatedBySid = new $collectionClassName;
        $this->collLijstjespermssRelatedBySid->setModel('\Lijstjesperms');
    }

    /**
     * Gets an array of ChildLijstjesperms objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLijstjesperms[] List of ChildLijstjesperms objects
     * @throws PropelException
     */
    public function getLijstjespermssRelatedBySid(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLijstjespermssRelatedBySidPartial && !$this->isNew();
        if (null === $this->collLijstjespermssRelatedBySid || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLijstjespermssRelatedBySid) {
                // return empty collection
                $this->initLijstjespermssRelatedBySid();
            } else {
                $collLijstjespermssRelatedBySid = ChildLijstjespermsQuery::create(null, $criteria)
                    ->filterByUsersRelatedBySid($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLijstjespermssRelatedBySidPartial && count($collLijstjespermssRelatedBySid)) {
                        $this->initLijstjespermssRelatedBySid(false);

                        foreach ($collLijstjespermssRelatedBySid as $obj) {
                            if (false == $this->collLijstjespermssRelatedBySid->contains($obj)) {
                                $this->collLijstjespermssRelatedBySid->append($obj);
                            }
                        }

                        $this->collLijstjespermssRelatedBySidPartial = true;
                    }

                    return $collLijstjespermssRelatedBySid;
                }

                if ($partial && $this->collLijstjespermssRelatedBySid) {
                    foreach ($this->collLijstjespermssRelatedBySid as $obj) {
                        if ($obj->isNew()) {
                            $collLijstjespermssRelatedBySid[] = $obj;
                        }
                    }
                }

                $this->collLijstjespermssRelatedBySid = $collLijstjespermssRelatedBySid;
                $this->collLijstjespermssRelatedBySidPartial = false;
            }
        }

        return $this->collLijstjespermssRelatedBySid;
    }

    /**
     * Sets a collection of ChildLijstjesperms objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lijstjespermssRelatedBySid A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setLijstjespermssRelatedBySid(Collection $lijstjespermssRelatedBySid, ConnectionInterface $con = null)
    {
        /** @var ChildLijstjesperms[] $lijstjespermssRelatedBySidToDelete */
        $lijstjespermssRelatedBySidToDelete = $this->getLijstjespermssRelatedBySid(new Criteria(), $con)->diff($lijstjespermssRelatedBySid);


        $this->lijstjespermssRelatedBySidScheduledForDeletion = $lijstjespermssRelatedBySidToDelete;

        foreach ($lijstjespermssRelatedBySidToDelete as $lijstjespermsRelatedBySidRemoved) {
            $lijstjespermsRelatedBySidRemoved->setUsersRelatedBySid(null);
        }

        $this->collLijstjespermssRelatedBySid = null;
        foreach ($lijstjespermssRelatedBySid as $lijstjespermsRelatedBySid) {
            $this->addLijstjespermsRelatedBySid($lijstjespermsRelatedBySid);
        }

        $this->collLijstjespermssRelatedBySid = $lijstjespermssRelatedBySid;
        $this->collLijstjespermssRelatedBySidPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Lijstjesperms objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Lijstjesperms objects.
     * @throws PropelException
     */
    public function countLijstjespermssRelatedBySid(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLijstjespermssRelatedBySidPartial && !$this->isNew();
        if (null === $this->collLijstjespermssRelatedBySid || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLijstjespermssRelatedBySid) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLijstjespermssRelatedBySid());
            }

            $query = ChildLijstjespermsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsersRelatedBySid($this)
                ->count($con);
        }

        return count($this->collLijstjespermssRelatedBySid);
    }

    /**
     * Method called to associate a ChildLijstjesperms object to this object
     * through the ChildLijstjesperms foreign key attribute.
     *
     * @param  ChildLijstjesperms $l ChildLijstjesperms
     * @return $this|\Users The current object (for fluent API support)
     */
    public function addLijstjespermsRelatedBySid(ChildLijstjesperms $l)
    {
        if ($this->collLijstjespermssRelatedBySid === null) {
            $this->initLijstjespermssRelatedBySid();
            $this->collLijstjespermssRelatedBySidPartial = true;
        }

        if (!$this->collLijstjespermssRelatedBySid->contains($l)) {
            $this->doAddLijstjespermsRelatedBySid($l);

            if ($this->lijstjespermssRelatedBySidScheduledForDeletion and $this->lijstjespermssRelatedBySidScheduledForDeletion->contains($l)) {
                $this->lijstjespermssRelatedBySidScheduledForDeletion->remove($this->lijstjespermssRelatedBySidScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLijstjesperms $lijstjespermsRelatedBySid The ChildLijstjesperms object to add.
     */
    protected function doAddLijstjespermsRelatedBySid(ChildLijstjesperms $lijstjespermsRelatedBySid)
    {
        $this->collLijstjespermssRelatedBySid[]= $lijstjespermsRelatedBySid;
        $lijstjespermsRelatedBySid->setUsersRelatedBySid($this);
    }

    /**
     * @param  ChildLijstjesperms $lijstjespermsRelatedBySid The ChildLijstjesperms object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeLijstjespermsRelatedBySid(ChildLijstjesperms $lijstjespermsRelatedBySid)
    {
        if ($this->getLijstjespermssRelatedBySid()->contains($lijstjespermsRelatedBySid)) {
            $pos = $this->collLijstjespermssRelatedBySid->search($lijstjespermsRelatedBySid);
            $this->collLijstjespermssRelatedBySid->remove($pos);
            if (null === $this->lijstjespermssRelatedBySidScheduledForDeletion) {
                $this->lijstjespermssRelatedBySidScheduledForDeletion = clone $this->collLijstjespermssRelatedBySid;
                $this->lijstjespermssRelatedBySidScheduledForDeletion->clear();
            }
            $this->lijstjespermssRelatedBySidScheduledForDeletion[]= $lijstjespermsRelatedBySid;
            $lijstjespermsRelatedBySid->setUsersRelatedBySid(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Users is new, it will return
     * an empty collection; or if this Users has previously
     * been saved, it will retrieve related LijstjespermssRelatedBySid from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Users.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLijstjesperms[] List of ChildLijstjesperms objects
     */
    public function getLijstjespermssRelatedBySidJoinLijstjes(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLijstjespermsQuery::create(null, $criteria);
        $query->joinWith('Lijstjes', $joinBehavior);

        return $this->getLijstjespermssRelatedBySid($query, $con);
    }

    /**
     * Clears out the collActivationtokenss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addActivationtokenss()
     */
    public function clearActivationtokenss()
    {
        $this->collActivationtokenss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collActivationtokenss collection loaded partially.
     */
    public function resetPartialActivationtokenss($v = true)
    {
        $this->collActivationtokenssPartial = $v;
    }

    /**
     * Initializes the collActivationtokenss collection.
     *
     * By default this just sets the collActivationtokenss collection to an empty array (like clearcollActivationtokenss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initActivationtokenss($overrideExisting = true)
    {
        if (null !== $this->collActivationtokenss && !$overrideExisting) {
            return;
        }

        $collectionClassName = ActivationtokensTableMap::getTableMap()->getCollectionClassName();

        $this->collActivationtokenss = new $collectionClassName;
        $this->collActivationtokenss->setModel('\Activationtokens');
    }

    /**
     * Gets an array of ChildActivationtokens objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildActivationtokens[] List of ChildActivationtokens objects
     * @throws PropelException
     */
    public function getActivationtokenss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collActivationtokenssPartial && !$this->isNew();
        if (null === $this->collActivationtokenss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collActivationtokenss) {
                // return empty collection
                $this->initActivationtokenss();
            } else {
                $collActivationtokenss = ChildActivationtokensQuery::create(null, $criteria)
                    ->filterByUsers($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collActivationtokenssPartial && count($collActivationtokenss)) {
                        $this->initActivationtokenss(false);

                        foreach ($collActivationtokenss as $obj) {
                            if (false == $this->collActivationtokenss->contains($obj)) {
                                $this->collActivationtokenss->append($obj);
                            }
                        }

                        $this->collActivationtokenssPartial = true;
                    }

                    return $collActivationtokenss;
                }

                if ($partial && $this->collActivationtokenss) {
                    foreach ($this->collActivationtokenss as $obj) {
                        if ($obj->isNew()) {
                            $collActivationtokenss[] = $obj;
                        }
                    }
                }

                $this->collActivationtokenss = $collActivationtokenss;
                $this->collActivationtokenssPartial = false;
            }
        }

        return $this->collActivationtokenss;
    }

    /**
     * Sets a collection of ChildActivationtokens objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $activationtokenss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setActivationtokenss(Collection $activationtokenss, ConnectionInterface $con = null)
    {
        /** @var ChildActivationtokens[] $activationtokenssToDelete */
        $activationtokenssToDelete = $this->getActivationtokenss(new Criteria(), $con)->diff($activationtokenss);


        $this->activationtokenssScheduledForDeletion = $activationtokenssToDelete;

        foreach ($activationtokenssToDelete as $activationtokensRemoved) {
            $activationtokensRemoved->setUsers(null);
        }

        $this->collActivationtokenss = null;
        foreach ($activationtokenss as $activationtokens) {
            $this->addActivationtokens($activationtokens);
        }

        $this->collActivationtokenss = $activationtokenss;
        $this->collActivationtokenssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Activationtokens objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Activationtokens objects.
     * @throws PropelException
     */
    public function countActivationtokenss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collActivationtokenssPartial && !$this->isNew();
        if (null === $this->collActivationtokenss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collActivationtokenss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getActivationtokenss());
            }

            $query = ChildActivationtokensQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsers($this)
                ->count($con);
        }

        return count($this->collActivationtokenss);
    }

    /**
     * Method called to associate a ChildActivationtokens object to this object
     * through the ChildActivationtokens foreign key attribute.
     *
     * @param  ChildActivationtokens $l ChildActivationtokens
     * @return $this|\Users The current object (for fluent API support)
     */
    public function addActivationtokens(ChildActivationtokens $l)
    {
        if ($this->collActivationtokenss === null) {
            $this->initActivationtokenss();
            $this->collActivationtokenssPartial = true;
        }

        if (!$this->collActivationtokenss->contains($l)) {
            $this->doAddActivationtokens($l);

            if ($this->activationtokenssScheduledForDeletion and $this->activationtokenssScheduledForDeletion->contains($l)) {
                $this->activationtokenssScheduledForDeletion->remove($this->activationtokenssScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildActivationtokens $activationtokens The ChildActivationtokens object to add.
     */
    protected function doAddActivationtokens(ChildActivationtokens $activationtokens)
    {
        $this->collActivationtokenss[]= $activationtokens;
        $activationtokens->setUsers($this);
    }

    /**
     * @param  ChildActivationtokens $activationtokens The ChildActivationtokens object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeActivationtokens(ChildActivationtokens $activationtokens)
    {
        if ($this->getActivationtokenss()->contains($activationtokens)) {
            $pos = $this->collActivationtokenss->search($activationtokens);
            $this->collActivationtokenss->remove($pos);
            if (null === $this->activationtokenssScheduledForDeletion) {
                $this->activationtokenssScheduledForDeletion = clone $this->collActivationtokenss;
                $this->activationtokenssScheduledForDeletion->clear();
            }
            $this->activationtokenssScheduledForDeletion[]= $activationtokens;
            $activationtokens->setUsers(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->voornaam = null;
        $this->achternaam = null;
        $this->username = null;
        $this->email = null;
        $this->password = null;
        $this->isactivated = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collAuthtokenss) {
                foreach ($this->collAuthtokenss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLijstjess) {
                foreach ($this->collLijstjess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLijstjespermssRelatedByOid) {
                foreach ($this->collLijstjespermssRelatedByOid as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLijstjespermssRelatedBySid) {
                foreach ($this->collLijstjespermssRelatedBySid as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collActivationtokenss) {
                foreach ($this->collActivationtokenss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collAuthtokenss = null;
        $this->collLijstjess = null;
        $this->collLijstjespermssRelatedByOid = null;
        $this->collLijstjespermssRelatedBySid = null;
        $this->collActivationtokenss = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UsersTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
