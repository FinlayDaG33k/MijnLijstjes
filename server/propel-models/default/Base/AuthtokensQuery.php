<?php

namespace Base;

use \Authtokens as ChildAuthtokens;
use \AuthtokensQuery as ChildAuthtokensQuery;
use \Exception;
use \PDO;
use Map\AuthtokensTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'Authtokens' table.
 *
 *
 *
 * @method     ChildAuthtokensQuery orderByTid($order = Criteria::ASC) Order by the TID column
 * @method     ChildAuthtokensQuery orderByToken($order = Criteria::ASC) Order by the Token column
 * @method     ChildAuthtokensQuery orderByLastseen($order = Criteria::ASC) Order by the LastSeen column
 * @method     ChildAuthtokensQuery orderByUid($order = Criteria::ASC) Order by the UID column
 *
 * @method     ChildAuthtokensQuery groupByTid() Group by the TID column
 * @method     ChildAuthtokensQuery groupByToken() Group by the Token column
 * @method     ChildAuthtokensQuery groupByLastseen() Group by the LastSeen column
 * @method     ChildAuthtokensQuery groupByUid() Group by the UID column
 *
 * @method     ChildAuthtokensQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAuthtokensQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAuthtokensQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAuthtokensQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAuthtokensQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAuthtokensQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAuthtokensQuery leftJoinUsers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Users relation
 * @method     ChildAuthtokensQuery rightJoinUsers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Users relation
 * @method     ChildAuthtokensQuery innerJoinUsers($relationAlias = null) Adds a INNER JOIN clause to the query using the Users relation
 *
 * @method     ChildAuthtokensQuery joinWithUsers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Users relation
 *
 * @method     ChildAuthtokensQuery leftJoinWithUsers() Adds a LEFT JOIN clause and with to the query using the Users relation
 * @method     ChildAuthtokensQuery rightJoinWithUsers() Adds a RIGHT JOIN clause and with to the query using the Users relation
 * @method     ChildAuthtokensQuery innerJoinWithUsers() Adds a INNER JOIN clause and with to the query using the Users relation
 *
 * @method     \UsersQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildAuthtokens findOne(ConnectionInterface $con = null) Return the first ChildAuthtokens matching the query
 * @method     ChildAuthtokens findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAuthtokens matching the query, or a new ChildAuthtokens object populated from the query conditions when no match is found
 *
 * @method     ChildAuthtokens findOneByTid(int $TID) Return the first ChildAuthtokens filtered by the TID column
 * @method     ChildAuthtokens findOneByToken(string $Token) Return the first ChildAuthtokens filtered by the Token column
 * @method     ChildAuthtokens findOneByLastseen(int $LastSeen) Return the first ChildAuthtokens filtered by the LastSeen column
 * @method     ChildAuthtokens findOneByUid(int $UID) Return the first ChildAuthtokens filtered by the UID column *

 * @method     ChildAuthtokens requirePk($key, ConnectionInterface $con = null) Return the ChildAuthtokens by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthtokens requireOne(ConnectionInterface $con = null) Return the first ChildAuthtokens matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAuthtokens requireOneByTid(int $TID) Return the first ChildAuthtokens filtered by the TID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthtokens requireOneByToken(string $Token) Return the first ChildAuthtokens filtered by the Token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthtokens requireOneByLastseen(int $LastSeen) Return the first ChildAuthtokens filtered by the LastSeen column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthtokens requireOneByUid(int $UID) Return the first ChildAuthtokens filtered by the UID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAuthtokens[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAuthtokens objects based on current ModelCriteria
 * @method     ChildAuthtokens[]|ObjectCollection findByTid(int $TID) Return ChildAuthtokens objects filtered by the TID column
 * @method     ChildAuthtokens[]|ObjectCollection findByToken(string $Token) Return ChildAuthtokens objects filtered by the Token column
 * @method     ChildAuthtokens[]|ObjectCollection findByLastseen(int $LastSeen) Return ChildAuthtokens objects filtered by the LastSeen column
 * @method     ChildAuthtokens[]|ObjectCollection findByUid(int $UID) Return ChildAuthtokens objects filtered by the UID column
 * @method     ChildAuthtokens[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AuthtokensQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AuthtokensQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Authtokens', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAuthtokensQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAuthtokensQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAuthtokensQuery) {
            return $criteria;
        }
        $query = new ChildAuthtokensQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAuthtokens|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AuthtokensTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AuthtokensTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAuthtokens A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT TID, Token, LastSeen, UID FROM Authtokens WHERE TID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAuthtokens $obj */
            $obj = new ChildAuthtokens();
            $obj->hydrate($row);
            AuthtokensTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAuthtokens|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AuthtokensTableMap::COL_TID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AuthtokensTableMap::COL_TID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the TID column
     *
     * Example usage:
     * <code>
     * $query->filterByTid(1234); // WHERE TID = 1234
     * $query->filterByTid(array(12, 34)); // WHERE TID IN (12, 34)
     * $query->filterByTid(array('min' => 12)); // WHERE TID > 12
     * </code>
     *
     * @param     mixed $tid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function filterByTid($tid = null, $comparison = null)
    {
        if (is_array($tid)) {
            $useMinMax = false;
            if (isset($tid['min'])) {
                $this->addUsingAlias(AuthtokensTableMap::COL_TID, $tid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tid['max'])) {
                $this->addUsingAlias(AuthtokensTableMap::COL_TID, $tid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AuthtokensTableMap::COL_TID, $tid, $comparison);
    }

    /**
     * Filter the query on the Token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE Token = 'fooValue'
     * $query->filterByToken('%fooValue%', Criteria::LIKE); // WHERE Token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AuthtokensTableMap::COL_TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the LastSeen column
     *
     * Example usage:
     * <code>
     * $query->filterByLastseen(1234); // WHERE LastSeen = 1234
     * $query->filterByLastseen(array(12, 34)); // WHERE LastSeen IN (12, 34)
     * $query->filterByLastseen(array('min' => 12)); // WHERE LastSeen > 12
     * </code>
     *
     * @param     mixed $lastseen The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function filterByLastseen($lastseen = null, $comparison = null)
    {
        if (is_array($lastseen)) {
            $useMinMax = false;
            if (isset($lastseen['min'])) {
                $this->addUsingAlias(AuthtokensTableMap::COL_LASTSEEN, $lastseen['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastseen['max'])) {
                $this->addUsingAlias(AuthtokensTableMap::COL_LASTSEEN, $lastseen['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AuthtokensTableMap::COL_LASTSEEN, $lastseen, $comparison);
    }

    /**
     * Filter the query on the UID column
     *
     * Example usage:
     * <code>
     * $query->filterByUid(1234); // WHERE UID = 1234
     * $query->filterByUid(array(12, 34)); // WHERE UID IN (12, 34)
     * $query->filterByUid(array('min' => 12)); // WHERE UID > 12
     * </code>
     *
     * @see       filterByUsers()
     *
     * @param     mixed $uid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function filterByUid($uid = null, $comparison = null)
    {
        if (is_array($uid)) {
            $useMinMax = false;
            if (isset($uid['min'])) {
                $this->addUsingAlias(AuthtokensTableMap::COL_UID, $uid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uid['max'])) {
                $this->addUsingAlias(AuthtokensTableMap::COL_UID, $uid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AuthtokensTableMap::COL_UID, $uid, $comparison);
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAuthtokensQuery The current query, for fluid interface
     */
    public function filterByUsers($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(AuthtokensTableMap::COL_UID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AuthtokensTableMap::COL_UID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsers() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Users relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function joinUsers($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Users');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Users');
        }

        return $this;
    }

    /**
     * Use the Users relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Users', '\UsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAuthtokens $authtokens Object to remove from the list of results
     *
     * @return $this|ChildAuthtokensQuery The current query, for fluid interface
     */
    public function prune($authtokens = null)
    {
        if ($authtokens) {
            $this->addUsingAlias(AuthtokensTableMap::COL_TID, $authtokens->getTid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Authtokens table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthtokensTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AuthtokensTableMap::clearInstancePool();
            AuthtokensTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthtokensTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AuthtokensTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AuthtokensTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AuthtokensTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AuthtokensQuery
