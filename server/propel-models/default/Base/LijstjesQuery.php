<?php

namespace Base;

use \Lijstjes as ChildLijstjes;
use \LijstjesQuery as ChildLijstjesQuery;
use \Exception;
use \PDO;
use Map\LijstjesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'Lijstjes' table.
 *
 *
 *
 * @method     ChildLijstjesQuery orderByLid($order = Criteria::ASC) Order by the LID column
 * @method     ChildLijstjesQuery orderByName($order = Criteria::ASC) Order by the Name column
 * @method     ChildLijstjesQuery orderByData($order = Criteria::ASC) Order by the Data column
 * @method     ChildLijstjesQuery orderByOid($order = Criteria::ASC) Order by the OID column
 *
 * @method     ChildLijstjesQuery groupByLid() Group by the LID column
 * @method     ChildLijstjesQuery groupByName() Group by the Name column
 * @method     ChildLijstjesQuery groupByData() Group by the Data column
 * @method     ChildLijstjesQuery groupByOid() Group by the OID column
 *
 * @method     ChildLijstjesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLijstjesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLijstjesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLijstjesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLijstjesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLijstjesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLijstjesQuery leftJoinUsers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Users relation
 * @method     ChildLijstjesQuery rightJoinUsers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Users relation
 * @method     ChildLijstjesQuery innerJoinUsers($relationAlias = null) Adds a INNER JOIN clause to the query using the Users relation
 *
 * @method     ChildLijstjesQuery joinWithUsers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Users relation
 *
 * @method     ChildLijstjesQuery leftJoinWithUsers() Adds a LEFT JOIN clause and with to the query using the Users relation
 * @method     ChildLijstjesQuery rightJoinWithUsers() Adds a RIGHT JOIN clause and with to the query using the Users relation
 * @method     ChildLijstjesQuery innerJoinWithUsers() Adds a INNER JOIN clause and with to the query using the Users relation
 *
 * @method     ChildLijstjesQuery leftJoinLijstjesperms($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lijstjesperms relation
 * @method     ChildLijstjesQuery rightJoinLijstjesperms($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lijstjesperms relation
 * @method     ChildLijstjesQuery innerJoinLijstjesperms($relationAlias = null) Adds a INNER JOIN clause to the query using the Lijstjesperms relation
 *
 * @method     ChildLijstjesQuery joinWithLijstjesperms($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lijstjesperms relation
 *
 * @method     ChildLijstjesQuery leftJoinWithLijstjesperms() Adds a LEFT JOIN clause and with to the query using the Lijstjesperms relation
 * @method     ChildLijstjesQuery rightJoinWithLijstjesperms() Adds a RIGHT JOIN clause and with to the query using the Lijstjesperms relation
 * @method     ChildLijstjesQuery innerJoinWithLijstjesperms() Adds a INNER JOIN clause and with to the query using the Lijstjesperms relation
 *
 * @method     \UsersQuery|\LijstjespermsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLijstjes findOne(ConnectionInterface $con = null) Return the first ChildLijstjes matching the query
 * @method     ChildLijstjes findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLijstjes matching the query, or a new ChildLijstjes object populated from the query conditions when no match is found
 *
 * @method     ChildLijstjes findOneByLid(int $LID) Return the first ChildLijstjes filtered by the LID column
 * @method     ChildLijstjes findOneByName(string $Name) Return the first ChildLijstjes filtered by the Name column
 * @method     ChildLijstjes findOneByData(string $Data) Return the first ChildLijstjes filtered by the Data column
 * @method     ChildLijstjes findOneByOid(int $OID) Return the first ChildLijstjes filtered by the OID column *

 * @method     ChildLijstjes requirePk($key, ConnectionInterface $con = null) Return the ChildLijstjes by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjes requireOne(ConnectionInterface $con = null) Return the first ChildLijstjes matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLijstjes requireOneByLid(int $LID) Return the first ChildLijstjes filtered by the LID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjes requireOneByName(string $Name) Return the first ChildLijstjes filtered by the Name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjes requireOneByData(string $Data) Return the first ChildLijstjes filtered by the Data column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLijstjes requireOneByOid(int $OID) Return the first ChildLijstjes filtered by the OID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLijstjes[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLijstjes objects based on current ModelCriteria
 * @method     ChildLijstjes[]|ObjectCollection findByLid(int $LID) Return ChildLijstjes objects filtered by the LID column
 * @method     ChildLijstjes[]|ObjectCollection findByName(string $Name) Return ChildLijstjes objects filtered by the Name column
 * @method     ChildLijstjes[]|ObjectCollection findByData(string $Data) Return ChildLijstjes objects filtered by the Data column
 * @method     ChildLijstjes[]|ObjectCollection findByOid(int $OID) Return ChildLijstjes objects filtered by the OID column
 * @method     ChildLijstjes[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LijstjesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\LijstjesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Lijstjes', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLijstjesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLijstjesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLijstjesQuery) {
            return $criteria;
        }
        $query = new ChildLijstjesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLijstjes|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LijstjesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LijstjesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLijstjes A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT LID, Name, Data, OID FROM Lijstjes WHERE LID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLijstjes $obj */
            $obj = new ChildLijstjes();
            $obj->hydrate($row);
            LijstjesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLijstjes|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LijstjesTableMap::COL_LID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LijstjesTableMap::COL_LID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the LID column
     *
     * Example usage:
     * <code>
     * $query->filterByLid(1234); // WHERE LID = 1234
     * $query->filterByLid(array(12, 34)); // WHERE LID IN (12, 34)
     * $query->filterByLid(array('min' => 12)); // WHERE LID > 12
     * </code>
     *
     * @param     mixed $lid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByLid($lid = null, $comparison = null)
    {
        if (is_array($lid)) {
            $useMinMax = false;
            if (isset($lid['min'])) {
                $this->addUsingAlias(LijstjesTableMap::COL_LID, $lid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lid['max'])) {
                $this->addUsingAlias(LijstjesTableMap::COL_LID, $lid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjesTableMap::COL_LID, $lid, $comparison);
    }

    /**
     * Filter the query on the Name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE Name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE Name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjesTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the Data column
     *
     * Example usage:
     * <code>
     * $query->filterByData('fooValue');   // WHERE Data = 'fooValue'
     * $query->filterByData('%fooValue%', Criteria::LIKE); // WHERE Data LIKE '%fooValue%'
     * </code>
     *
     * @param     string $data The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByData($data = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($data)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjesTableMap::COL_DATA, $data, $comparison);
    }

    /**
     * Filter the query on the OID column
     *
     * Example usage:
     * <code>
     * $query->filterByOid(1234); // WHERE OID = 1234
     * $query->filterByOid(array(12, 34)); // WHERE OID IN (12, 34)
     * $query->filterByOid(array('min' => 12)); // WHERE OID > 12
     * </code>
     *
     * @see       filterByUsers()
     *
     * @param     mixed $oid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByOid($oid = null, $comparison = null)
    {
        if (is_array($oid)) {
            $useMinMax = false;
            if (isset($oid['min'])) {
                $this->addUsingAlias(LijstjesTableMap::COL_OID, $oid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oid['max'])) {
                $this->addUsingAlias(LijstjesTableMap::COL_OID, $oid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LijstjesTableMap::COL_OID, $oid, $comparison);
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByUsers($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(LijstjesTableMap::COL_OID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LijstjesTableMap::COL_OID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsers() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Users relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function joinUsers($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Users');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Users');
        }

        return $this;
    }

    /**
     * Use the Users relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Users', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Lijstjesperms object
     *
     * @param \Lijstjesperms|ObjectCollection $lijstjesperms the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLijstjesQuery The current query, for fluid interface
     */
    public function filterByLijstjesperms($lijstjesperms, $comparison = null)
    {
        if ($lijstjesperms instanceof \Lijstjesperms) {
            return $this
                ->addUsingAlias(LijstjesTableMap::COL_LID, $lijstjesperms->getLid(), $comparison);
        } elseif ($lijstjesperms instanceof ObjectCollection) {
            return $this
                ->useLijstjespermsQuery()
                ->filterByPrimaryKeys($lijstjesperms->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLijstjesperms() only accepts arguments of type \Lijstjesperms or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lijstjesperms relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function joinLijstjesperms($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lijstjesperms');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lijstjesperms');
        }

        return $this;
    }

    /**
     * Use the Lijstjesperms relation Lijstjesperms object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LijstjespermsQuery A secondary query class using the current class as primary query
     */
    public function useLijstjespermsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLijstjesperms($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lijstjesperms', '\LijstjespermsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLijstjes $lijstjes Object to remove from the list of results
     *
     * @return $this|ChildLijstjesQuery The current query, for fluid interface
     */
    public function prune($lijstjes = null)
    {
        if ($lijstjes) {
            $this->addUsingAlias(LijstjesTableMap::COL_LID, $lijstjes->getLid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Lijstjes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LijstjesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LijstjesTableMap::clearInstancePool();
            LijstjesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LijstjesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LijstjesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LijstjesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LijstjesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LijstjesQuery
