<?php

namespace Map;

use \Lijstjesperms;
use \LijstjespermsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'LijstjesPerms' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class LijstjespermsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'default.Map.LijstjespermsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'LijstjesPerms';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Lijstjesperms';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'default.Lijstjesperms';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the ID field
     */
    const COL_ID = 'LijstjesPerms.ID';

    /**
     * the column name for the R field
     */
    const COL_R = 'LijstjesPerms.R';

    /**
     * the column name for the A field
     */
    const COL_A = 'LijstjesPerms.A';

    /**
     * the column name for the E field
     */
    const COL_E = 'LijstjesPerms.E';

    /**
     * the column name for the S field
     */
    const COL_S = 'LijstjesPerms.S';

    /**
     * the column name for the D field
     */
    const COL_D = 'LijstjesPerms.D';

    /**
     * the column name for the OID field
     */
    const COL_OID = 'LijstjesPerms.OID';

    /**
     * the column name for the SID field
     */
    const COL_SID = 'LijstjesPerms.SID';

    /**
     * the column name for the LID field
     */
    const COL_LID = 'LijstjesPerms.LID';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'R', 'A', 'E', 'S', 'D', 'Oid', 'Sid', 'Lid', ),
        self::TYPE_CAMELNAME     => array('id', 'r', 'a', 'e', 's', 'd', 'oid', 'sid', 'lid', ),
        self::TYPE_COLNAME       => array(LijstjespermsTableMap::COL_ID, LijstjespermsTableMap::COL_R, LijstjespermsTableMap::COL_A, LijstjespermsTableMap::COL_E, LijstjespermsTableMap::COL_S, LijstjespermsTableMap::COL_D, LijstjespermsTableMap::COL_OID, LijstjespermsTableMap::COL_SID, LijstjespermsTableMap::COL_LID, ),
        self::TYPE_FIELDNAME     => array('ID', 'R', 'A', 'E', 'S', 'D', 'OID', 'SID', 'LID', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'R' => 1, 'A' => 2, 'E' => 3, 'S' => 4, 'D' => 5, 'Oid' => 6, 'Sid' => 7, 'Lid' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'r' => 1, 'a' => 2, 'e' => 3, 's' => 4, 'd' => 5, 'oid' => 6, 'sid' => 7, 'lid' => 8, ),
        self::TYPE_COLNAME       => array(LijstjespermsTableMap::COL_ID => 0, LijstjespermsTableMap::COL_R => 1, LijstjespermsTableMap::COL_A => 2, LijstjespermsTableMap::COL_E => 3, LijstjespermsTableMap::COL_S => 4, LijstjespermsTableMap::COL_D => 5, LijstjespermsTableMap::COL_OID => 6, LijstjespermsTableMap::COL_SID => 7, LijstjespermsTableMap::COL_LID => 8, ),
        self::TYPE_FIELDNAME     => array('ID' => 0, 'R' => 1, 'A' => 2, 'E' => 3, 'S' => 4, 'D' => 5, 'OID' => 6, 'SID' => 7, 'LID' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('LijstjesPerms');
        $this->setPhpName('Lijstjesperms');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Lijstjesperms');
        $this->setPackage('default');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('R', 'R', 'INTEGER', true, null, null);
        $this->addColumn('A', 'A', 'INTEGER', true, 1, null);
        $this->addColumn('E', 'E', 'INTEGER', true, 1, null);
        $this->addColumn('S', 'S', 'INTEGER', true, 1, null);
        $this->addColumn('D', 'D', 'INTEGER', true, 1, null);
        $this->addForeignKey('OID', 'Oid', 'INTEGER', 'Users', 'ID', false, null, null);
        $this->addForeignKey('SID', 'Sid', 'INTEGER', 'Users', 'ID', false, null, null);
        $this->addForeignKey('LID', 'Lid', 'INTEGER', 'Lijstjes', 'LID', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UsersRelatedByOid', '\\Users', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':OID',
    1 => ':ID',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('UsersRelatedBySid', '\\Users', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':SID',
    1 => ':ID',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Lijstjes', '\\Lijstjes', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':LID',
    1 => ':LID',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? LijstjespermsTableMap::CLASS_DEFAULT : LijstjespermsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Lijstjesperms object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = LijstjespermsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = LijstjespermsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + LijstjespermsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = LijstjespermsTableMap::OM_CLASS;
            /** @var Lijstjesperms $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            LijstjespermsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = LijstjespermsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = LijstjespermsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Lijstjesperms $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                LijstjespermsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_ID);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_R);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_A);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_E);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_S);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_D);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_OID);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_SID);
            $criteria->addSelectColumn(LijstjespermsTableMap::COL_LID);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.R');
            $criteria->addSelectColumn($alias . '.A');
            $criteria->addSelectColumn($alias . '.E');
            $criteria->addSelectColumn($alias . '.S');
            $criteria->addSelectColumn($alias . '.D');
            $criteria->addSelectColumn($alias . '.OID');
            $criteria->addSelectColumn($alias . '.SID');
            $criteria->addSelectColumn($alias . '.LID');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(LijstjespermsTableMap::DATABASE_NAME)->getTable(LijstjespermsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(LijstjespermsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(LijstjespermsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new LijstjespermsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Lijstjesperms or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Lijstjesperms object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LijstjespermsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Lijstjesperms) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(LijstjespermsTableMap::DATABASE_NAME);
            $criteria->add(LijstjespermsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = LijstjespermsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            LijstjespermsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                LijstjespermsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the LijstjesPerms table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return LijstjespermsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Lijstjesperms or Criteria object.
     *
     * @param mixed               $criteria Criteria or Lijstjesperms object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LijstjespermsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Lijstjesperms object
        }

        if ($criteria->containsKey(LijstjespermsTableMap::COL_ID) && $criteria->keyContainsValue(LijstjespermsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.LijstjespermsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = LijstjespermsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // LijstjespermsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
LijstjespermsTableMap::buildTableMap();
