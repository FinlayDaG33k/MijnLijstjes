<?php

use Base\LijstjesQuery as BaseLijstjesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Lijstjes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LijstjesQuery extends BaseLijstjesQuery
{

}
