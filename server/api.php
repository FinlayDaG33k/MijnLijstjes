<?php
  require("lib/Class-FinlayDaG33k/FinlayDaG33k.class.php");

  //Load composer's autoloader
  require 'vendor/autoload.php';

  // Load the configs
  include("config.inc.php");
  require("propel.config.php");

  if(empty($appConfig)){
    $apiOutput = array("status"=>666,"message"=>"server config not found!");
    die(json_encode($apiOutput));
  }

  include("loadMail.php");

  switch($FinlayDaG33k->EzServer->getMethod()){
    case "POST":
      include('methods/POST.php');
      break;
    case "GET":
      include('methods/GET.php');
      break;
    default:
      $apiOutput = array("status"=>400,"message"=>"No methods available for request method \"" .htmlentities($FinlayDaG33k->EzServer->getMethod()). "\"");
      break;
  }

  echo json_encode($apiOutput);
