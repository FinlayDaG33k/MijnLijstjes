--
-- Users table
--
CREATE TABLE `Users` (
  `ID` int(11) NOT NULL,
  `Voornaam` varchar(255) NOT NULL,
  `Achternaam` varchar(255) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `isActivated` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `Users` ADD PRIMARY KEY (`ID`);
ALTER TABLE `Users` MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

--
-- Authtokens Table
--
CREATE TABLE `Authtokens` (
  `TID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `Token` varchar(255) NOT NULL,
  `LastSeen` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `Authtokens` ADD PRIMARY KEY (`TID`);
ALTER TABLE `Authtokens` MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

--
-- Lijstjes Table
--
CREATE TABLE `Lijstjes` (
  `LID` int(11) NOT NULL COMMENT 'List ID',
  `OID` int(11) NOT NULL COMMENT 'Owner ID',
  `Name` varchar(255) NOT NULL,
  `Data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `Lijstjes` ADD PRIMARY KEY (`LID`);
ALTER TABLE `Lijstjes` MODIFY `LID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'List ID';
COMMIT;

--
-- Lijstjes-Perms Table
--
CREATE TABLE `Lijstjes-Perms` (
  `ID` int(11) NOT NULL,
  `OID` int(11) NOT NULL COMMENT 'OwnerID',
  `SID` int(11) NOT NULL COMMENT 'SubjectID',
  `LID` int(11) NOT NULL COMMENT 'ListID',
  `R` int(11) NOT NULL COMMENT 'Read',
  `A` int(1) NOT NULL COMMENT 'Add',
  `E` int(1) NOT NULL COMMENT 'Edit',
  `S` int(1) NOT NULL COMMENT 'Stripe',
  `D` int(1) NOT NULL COMMENT 'Delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `Lijstjes-Perms` ADD PRIMARY KEY (`ID`);
ALTER TABLE `Lijstjes-Perms` MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

--
-- activationTokens table
--
CREATE TABLE `activationTokens` (
  `TID` int(11) NOT NULL,
  `UID` int(255) NOT NULL,
  `Token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `activationTokens` ADD PRIMARY KEY (`TID`);
ALTER TABLE `activationTokens` MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;
